<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['estudiante']) && isset($_GET['acudiente'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);
    $id_acudiente = base64_decode($_GET['acudiente']);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>matricula/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Hoja de matricula
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            <input type="hidden" value="<?= $id_estudiante ?>" name="id_estudiante">
                            <input type="hidden" value="<?= $id_acudiente ?>" name="id_acudiente">
                            <input type="hidden" value="<?= $id_log ?>" name="id_log">
                            <div class="row">
                                <div class="col-lg-4 form-group">
                                    <label>Numero de folio <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" minlength="1" maxlength="50" name="num_folios">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Numero de matricula <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" minlength="1" maxlength="50" name="num_mat">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <label>Se matricula en <span class="text-danger">*</span></label>
                                    <select name="matricula" class="form-control" required>
                                        <option value="" selected>Seleccione una opcion...</option>
                                        <option value="NURSERY">NURSERY</option>
                                        <option value="MATERNAL">MATERNAL</option>
                                        <option value="STEM">STEM</option>
                                        <option value="PREKINDER">PREKINDER</option>
                                    </select>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>Procedencia</label>
                                    <textarea name="procedencia" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="col-lg-6 form-group">
                                    <label>Observacion</label>
                                    <textarea name="observacion" cols="30" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="col-lg-12 form-group mt-2">
                                    <button class="btn btn-success btn-sm float-right" type="submit">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Generar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['id_acudiente'])) {
        $instancia->generarHojaMatriculaControl();
    }
}
