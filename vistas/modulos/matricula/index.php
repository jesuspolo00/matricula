<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$datos_estudiantes = $instancia->mostrarEstudiantesControl();
$permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Matricula
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No. Solicitud</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Estudiante</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_estudiantes as $estudiantes) {
                                    $id_estudiante = $estudiantes['id'];
                                    $nombre = $estudiantes['nom_est'];
                                    $autorizacion = $estudiantes['autorizo'];
                                    $documento = $estudiantes['documento'];
                                    $nombre_completo = $estudiantes['nombre'] . ' ' . $estudiantes['apellido'];
                                    $correo = $estudiantes['correo'];
                                    $telefono = $estudiantes['telefono'];
                                    $id_acudiente = $estudiantes['id_acudiente'];

                                    $ver_autorizacion = ($autorizacion == 'si') ? '' : 'd-none';

                                    $hoja_matricula = $instancia->mostrarHojaMatriculaControl($id_acudiente, $id_estudiante);

                                    if ($hoja_matricula['id'] == '') {
                                        $href = BASE_URL . 'matricula/hoja_matricula?acudiente=' . base64_encode($id_acudiente) . '&estudiante=' . base64_encode($id_estudiante);
                                        $title = 'Generar hoja de matricula';
                                        $target = '';
                                    } else {
                                        $href = BASE_URL . 'imprimir/hoja_matricula?acudiente=' . base64_encode($id_acudiente) . '&estudiante=' . base64_encode($id_estudiante);
                                        $title = 'Descargar hoja de matricula';
                                        $target = 'target="_blank"';
                                    }
                                ?>
                                    <tr class="text-center">
                                        <td><?= $id_estudiante ?></td>
                                        <td><?= $documento ?></td>
                                        <td><?= $nombre_completo ?></td>
                                        <td><?= $correo ?></td>
                                        <td><?= $telefono ?></td>
                                        <td><?= $nombre ?></td>
                                        <td>
                                            <a href="<?= BASE_URL ?>imprimir/ficha?estudiante=<?= base64_encode($id_estudiante) ?>&acudiente=<?= base64_encode($id_acudiente) ?>" class="btn btn-primary btn-sm" target="_blank" data-tooltip="tooltip" title="Descargar ficha de matricula" data-placement="bottom">
                                                <i class="fas fa-file-pdf"></i>
                                            </a>
                                        </td>
                                        <td class="<?= $ver_autorizacion ?>">
                                            <a href="<?= BASE_URL ?>imprimir/autorizacion?estudiante=<?= base64_encode($id_estudiante) ?>" target="_blank" class="btn btn-success btn-sm" data-tooltip="tooltip" title="Descargar autorizacion" data-placement="bottom">
                                                <i class="fas fa-clipboard-check"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= $href ?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="<?= $title ?>" data-placement="bottom" <?= $target ?>>
                                                <i class="fas fa-file-alt"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?= BASE_URL ?>matricula/documentos?estudiante=<?= base64_encode($id_estudiante) ?>&acudiente=<?= base64_encode($id_acudiente) ?>" class="btn btn-secondary btn-sm" data-tooltip="tooltip" title="Descargar documentos" data-placement="bottom">
                                                <i class="fas fa-images"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>