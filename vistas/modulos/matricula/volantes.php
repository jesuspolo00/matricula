<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$datos_estudiantes = $instancia->mostrarEstudiantesControl();
$permisos = $instancia_permiso->permisosUsuarioControl(2, 5, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Volantes de pago
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No. Solicitud</th>
                                    <th scope="col">Estudiante</th>
                                    <th scope="col">Documento acudiente</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_estudiantes as $estudiantes) {
                                    $id_estudiante = $estudiantes['id'];
                                    $nombre = $estudiantes['nom_est'];
                                    $autorizacion = $estudiantes['autorizo'];
                                    $documento = $estudiantes['documento'];
                                    $nombre_completo = $estudiantes['nombre'] . ' ' . $estudiantes['apellido'];
                                    $correo = $estudiantes['correo'];
                                    $telefono = $estudiantes['telefono'];
                                    $id_acudiente = $estudiantes['id_acudiente'];

                                    $volante = $instancia->volanteIdControl($id_estudiante);

                                    if ($volante['id'] == '') {
                                        $subir = '';
                                        $descargar = 'd-none';
                                    } else {
                                        $subir = 'd-none';
                                        $descargar = '';
                                    }
                                ?>
                                    <tr class="text-center">
                                        <td><?= $id_estudiante ?></td>
                                        <td><?= $nombre ?></td>
                                        <td><?= $documento ?></td>
                                        <td><?= $nombre_completo ?></td>
                                        <td><?= $correo ?></td>
                                        <td><?= $telefono ?></td>
                                        <td>
                                            <button class="btn btn-info btn-sm <?= $subir ?>" data-tooltip="tooltip" data-placement="bottom" title="Subir volante" data-toggle="modal" data-target="#vol_<?= $id_estudiante ?>">
                                                <i class="fas fa-receipt"></i>
                                            </button>
                                            <a href="<?= PUBLIC_PATH ?>upload/<?= $volante['nombre'] ?>" download class="btn btn-primary btn-sm <?= $descargar ?>" data-tooltip="tooltip" data-placement="bottom" title="Descargar volante">
                                                <i class="fas fa-receipt"></i>
                                            </a>
                                        </td>
                                    </tr>


                                    <!-- Modal -->
                                    <div class="modal fade" id="vol_<?= $id_estudiante ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLabel">Subir volante</h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" enctype="multipart/form-data">
                                                        <input type="hidden" value="<?= $id_log ?>" name="id_log">
                                                        <input type="hidden" value="<?= $nombre ?>" name="nombre">
                                                        <input type="hidden" value="<?= $id_estudiante ?>" name="id_estudiante">
                                                        <input type="hidden" value="<?= $id_acudiente ?>" name="id_acudiente">
                                                        <div class="form-group col-lg-12">
                                                            <label>Volante de pago<span class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input archivo" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="volante" accept=".png,.jpg,.jpeg,.pdf">
                                                                    <label class="custom-file-label" for="inputGroupFile01" id="nom_archivo"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 text-right p-2">
                                                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                                                <i class="fa fa-times"></i>
                                                                &nbsp;
                                                                Cerrar
                                                            </button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fa fa-save"></i>
                                                                &nbsp;
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_acudiente'])) {
    $instancia->subirVolanteControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/nom_archivo.js"></script>