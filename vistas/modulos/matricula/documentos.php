<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['estudiante']) && isset($_GET['acudiente'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);
    $id_acudiente = base64_decode($_GET['acudiente']);

    $documentos = $instancia->mostrarDocumentosControl($id_acudiente, $id_estudiante);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>matricula/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Documentos
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12 mt-2">
                                <div class="list-group">
                                    <?php
                                    if (count($documentos) <= 0) {
                                    ?>
                                        <h4 class="text-center">Documentos no registrados</h4>
                                        <?php
                                    } else {
                                        foreach ($documentos as $archivo) {
                                            $id = $archivo['id'];
                                            $nombre = $archivo['nombre'];
                                            $tipo = $archivo['tipo'];
                                        ?>
                                            <a href="<?= PUBLIC_PATH ?>upload/<?= $nombre ?>" download class="list-group-item list-group-item-action">
                                                <?= $tipo ?>
                                                &nbsp;
                                                <i class="fa fa-download float-right text-primary"></i>
                                            </a>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';
}
