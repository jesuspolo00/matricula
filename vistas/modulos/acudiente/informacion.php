<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}


if (isset($_GET['acudiente'])) {

    $id_acudiente = base64_decode($_GET['acudiente']);
    $detalles_prematricula = $instancia->detallePrematriculaControl($id_acudiente);


    $span = ($detalles_prematricula[0]['tipo_user'] == 1) ? '<span class="badge badge-danger mt-3">Acudiente antiguo</span>' : '<span class="badge badge-success mt-3">Acudiente nuevo</span>';

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>acudiente/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Informacion de prematricula
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row p-2">
                            <div class="form-group col-lg-4">
                                <label>Documento</label>
                                <input type="text" class="form-control" disabled value="<?= $detalles_prematricula[0]['documento'] ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Nombre completo</label>
                                <input type="text" class="form-control" disabled value="<?= $detalles_prematricula[0]['nombre'] . ' ' . $detalles_prematricula[0]['apellido'] ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Correo</label>
                                <input type="text" class="form-control" disabled value="<?= $detalles_prematricula[0]['correo'] ?>">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Telefono</label>
                                <input type="text" class="form-control" disabled value="<?= $detalles_prematricula[0]['telefono'] ?>">
                            </div>
                            <div class="col-lg-2 mt-4 text-center">
                                <?= $span ?>
                            </div>
                        </div>
                        <div class="table-responsive mt-2">
                            <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">No. solicitud</th>
                                        <th scope="col">Estudiante</th>
                                        <th scope="col">Autorizado</th>
                                        <th scope="col">Registrado</th>
                                        <th scope="col">Ficha matricula</th>
                                        <th scope="col">Documentos</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar text-lowercase">
                                    <?php
                                    if (count($detalles_prematricula) <= 1) {
                                    ?>
                                        <tr class="text-center">
                                            <td colspan="6">No hay datos para mostrar</td>
                                        </tr>
                                        <?php
                                    } else {
                                        foreach ($detalles_prematricula as $datos) {
                                            $id_solicitud = $datos['solicitud'];
                                            $estudiante = $datos['nom_est'];
                                            $autorizo = ($datos['autorizo'] == 'si') ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-times-circle text-danger"></i>';
                                            $registro = ($datos['registro'] == 'si') ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-times-circle text-danger"></i>';
                                            $ficha_matricula = ($datos['ficha_matricula'] == 'si') ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-times-circle text-danger"></i>';
                                            $documentos = ($datos['documentos'] == 'si') ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-times-circle text-danger"></i>';
                                        ?>
                                            <tr class="text-center">
                                                <td><?= $id_solicitud ?></td>
                                                <td><?= $estudiante ?></td>
                                                <td><?= $autorizo ?></td>
                                                <td><?= $registro ?></td>
                                                <td><?= $ficha_matricula ?></td>
                                                <td><?= $documentos ?></td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';
}
