<!--Agregar usuario-->
<div class="modal fade" id="agregar_usuario" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg p-2" role="document">
        <div class="modal-content">
            <form method="POST" id="form_enviar">
                <input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-primary font-weight-bold">Agregar acudiente</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row  p-3">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Documento</label>
                                <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="documento" id="doc_user" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre" id="nombre" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Apellido</label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="apellido" id="apellido">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Telefono</label>
                                <input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono" id="telefono">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Correo</label>
                                <input type="email" class="form-control" maxlength="50" minlength="1" name="correo" id="correo">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                        &nbsp;
                        Cerrar
                    </button>
                    <button type="submit" class="btn btn-success btn-sm" id="enviar_datos">
                        <i class="fa fa-save"></i>
                        &nbsp;
                        Registrar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>