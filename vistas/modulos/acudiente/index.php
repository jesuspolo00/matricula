<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlUsuarios::singleton_usuarios();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_usuarios = $instancia->mostrarUsuariosControl();
$datos_perfil = $instancia_perfil->mostrarPerfilesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Acudientes
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar acudiente</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">#</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Perfil</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_usuarios as $usuario) {
                                    $id_user = $usuario['id_user'];
                                    $nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento = $usuario['documento'];
                                    $correo = $usuario['correo'];
                                    $telefono = $usuario['telefono'];
                                    $user = $usuario['user'];
                                    $pass_old = $usuario['pass'];
                                    $nom_perfil = $usuario['nom_perfil'];
                                    $estado = $usuario['estado'];

                                    if ($estado != 'activo') {
                                        $class = 'btn-success btn-sm activar_user';
                                        $icon = '<i class="fa fa-check"></i>';
                                        $tooltip = 'Activar usuario';
                                    } else {
                                        $class = 'btn-danger btn-sm inactivar_user';
                                        $icon = '<i class="fa fa-times"></i>';
                                        $tooltip = 'Inactivar usuario';
                                    }

                                    if ($usuario['perfil'] == 2) {

                                ?>
                                        <tr class="text-center">
                                            <td><?= $id_user ?></td>
                                            <td><?= $documento ?></td>
                                            <td><?= $nombre_completo ?></td>
                                            <td><?= $correo ?></td>
                                            <td><?= $telefono ?></td>
                                            <td><?= $user ?></td>
                                            <td><?= $nom_perfil ?></td>
                                            <td>
                                                <a href="<?= BASE_URL ?>acudiente/informacion?acudiente=<?= base64_encode($id_user)  ?>" class="btn btn-info btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Informacion de prematricula">
                                                    <i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Editar" data-toggle="modal" data-target="#edit_<?= $id_user ?>">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn <?= $class ?>" id="<?= $id_user ?>" data-tooltip="tooltip" data-placement="bottom" title="<?= $tooltip ?>">
                                                    <?= $icon ?>
                                                </button>
                                            </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade" id="edit_<?= $id_user ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-primary font-weight-bold" id="exampleModalLongTitle">Editar acudiente</h5>
                                                    </div>
                                                    <form method="POST">
                                                        <input type="hidden" value="<?= $id_user ?>" name="id_acudiente">
                                                        <div class="modal-body">
                                                            <div class="col-lg-12 form-group">
                                                                <label>Documento</label>
                                                                <input type="text" name="documento_edit" value="<?= $documento ?>" class="form-control" required>
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>Nombre</label>
                                                                <input type="text" name="nombre_edit" value="<?= $usuario['nombre'] ?>" class="form-control" required>
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>Apellido</label>
                                                                <input type="text" name="apellido_edit" value="<?= $usuario['apellido'] ?>" class="form-control">
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>Correo</label>
                                                                <input type="text" name="correo_edit" value="<?= $usuario['correo'] ?>" class="form-control">
                                                            </div>
                                                            <div class="col-lg-12 form-group">
                                                                <label>Telefono</label>
                                                                <input type="text" name="telefono_edit" value="<?= $usuario['telefono'] ?>" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <div class="col-lg-12 text-right">
                                                                <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">
                                                                    <i class="fa fa-times"></i>
                                                                    &nbsp;
                                                                    Cerrar
                                                                </button>
                                                                &nbsp;
                                                                <button class="btn btn-success btn-sm" type="submit">
                                                                    <i class="fa fa-save"></i>
                                                                    &nbsp;
                                                                    Guardar
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'acudiente' . DS . 'agregarAcudiente.php';

if (isset($_POST['documento'])) {
    $instancia->agregarAcudienteControl();
}

if (isset($_POST['documento_edit'])) {
    $instancia->actualizarAcudienteControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/usuario/funcionesUsuario.js"></script>