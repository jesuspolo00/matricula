<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlUsuarios::singleton_usuarios();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_usuarios = $instancia->mostrarUsuariosControl();
$datos_perfil = $instancia_perfil->mostrarPerfilesControl();

$permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        <a href="<?= BASE_URL ?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-primary"></i>
                        </a>
                        &nbsp;
                        Usuarios
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_usuario">Agregar Usuario</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">#</th>
                                    <th scope="col">Documento</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Perfil</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_usuarios as $usuario) {
                                    $id_user = $usuario['id_user'];
                                    $nombre_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
                                    $documento = $usuario['documento'];
                                    $correo = $usuario['correo'];
                                    $telefono = $usuario['telefono'];
                                    $user = $usuario['user'];
                                    $pass_old = $usuario['pass'];
                                    $nom_perfil = $usuario['nom_perfil'];
                                    $estado = $usuario['estado'];

                                    if ($estado != 'activo') {
                                        $class = 'btn-success btn-sm activar_user';
                                        $icon = '<i class="fa fa-check"></i>';
                                        $tooltip = 'Activar usuario';
                                    } else {
                                        $class = 'btn-danger btn-sm inactivar_user';
                                        $icon = '<i class="fa fa-times"></i>';
                                        $tooltip = 'Inactivar usuario';
                                    }

                                ?>
                                    <tr class="text-center">
                                        <td><?= $id_user ?></td>
                                        <td><?= $documento ?></td>
                                        <td><?= $nombre_completo ?></td>
                                        <td><?= $correo ?></td>
                                        <td><?= $telefono ?></td>
                                        <td><?= $user ?></td>
                                        <td><?= $nom_perfil ?></td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Editar">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                        <td>
                                            <button class="btn <?= $class ?>" id="<?= $id_user ?>" data-tooltip="tooltip" data-placement="bottom" title="<?= $tooltip ?>">
                                                <?= $icon ?>
                                            </button>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['documento'])) {
    $instancia->agregarUsuarioControl();
}
?>
<script src="<?= PUBLIC_PATH ?>js/usuario/funcionesUsuario.js"></script>