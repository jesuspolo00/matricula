<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$datos_estudiantes = $instancia->mostrarEstudiantesControl();
$permisos = $instancia_permiso->permisosUsuarioControl(2, 6, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}

if (isset($_GET['estudiante']) && isset($_GET['acudiente'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);
    $id_acudiente = base64_decode($_GET['acudiente']);

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>contratos/index" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Generar contrato
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST">
                            <input type="hidden" name="id_estudiante" value="<?= $id_estudiante ?>">
                            <input type="hidden" name="id_acudiente" value="<?= $id_acudiente ?>">
                            <input type="hidden" name="id_log" value="<?= $id_log ?>">
                            <div class="row p-2">
                                <div class="form-group col-lg-6">
                                    <label>Valor del contrato</label>
                                    <select class="form-control" required="" name="tipo_contrato">
                                        <option value="" selected="">Seleccione una opcion...</option>
                                        <option value="1">Matricula normal</option>
                                        <option value="2">Matricula con descuento</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>No. pagare</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">#</span>
                                        </div>
                                        <input type="text" class="form-control numeros" name="pagare" minlength="1" maxlength="50">
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 text-right">
                                    <button class="btn btn-success btn-sm" type="submit">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['id_acudiente'])) {
        $instancia->generarContratoControl();
    }
    ?>
    <script src="<?= PUBLIC_PATH ?>js/contrato/funcionesContrato.js"></script>
<?php
}
