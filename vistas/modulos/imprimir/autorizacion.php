<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 10;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }

    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(100, 150, 56);
    }

    public function generar($id)
    {
        $instancia = ControlMatricula::singleton_matricula();
        $datos_estudiante = $instancia->mostrarDatosEstudianteIdControl($id);

        $nom_est = $datos_estudiante['nom_est'];
        $documento = $datos_estudiante['documento'];
        $nombre_completo = $datos_estudiante['nombre'] . ' ' . $datos_estudiante['apellido'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1($nombre_completo, $documento, $nom_est);
    }

    private function pagina1($nombre_completo, $documento, $nom_est)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/autorizacion/autorizacion_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // NOMBRE COMPLETO
        $this->SetXY(27, 129);
        $this->reducirT(80, 7, $nombre_completo, 1, 0, 'C');

        // CEDULAS
        $this->SetXY(147, 129);
        $this->reducirT(30, 7, $documento, 1, 0, 'C');

        // NOMBRE COMPLETO ESTUDIANTE
        $this->SetXY(65, 137);
        $this->reducirT(50, 7, $nom_est, 1, 0, 'C');
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Autorizacion", true);
$pdf->generar(base64_decode($_GET['estudiante']));
$pdf->Output();
