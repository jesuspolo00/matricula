<?php
setlocale(LC_ALL, "es_ES");
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';
require_once CONTROL_PATH . 'numeros.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 5;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }

    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(100, 150, 56);
    }

    public function generar($estudiante, $acudiente)
    {

        $instancia = ControlMatricula::singleton_matricula();
        $datos_contrato = $instancia->datosContratoControl($estudiante, $acudiente);

        $nom_padre = $datos_contrato['nom_padre'];
        $nom_madre = $datos_contrato['nom_madre'];
        $nom_est = $datos_contrato['estudiante'];
        $grado = $datos_contrato['grado'];
        $valor = $datos_contrato['valor'];
        $pagare = $datos_contrato['pagare'];
        $fecha_contrato = $datos_contrato['fecha_contrato'];
        $ced_padre = $datos_contrato['ced_padre'];
        $ced_madre = $datos_contrato['ced_madre'];
        $dir_resi = $datos_contrato['dir_resi'];
        $tel_resi = $datos_contrato['tel_resi'];
        $telefono = $datos_contrato['telefono'];
        $correo = $datos_contrato['correo'];
        $tipo = $datos_contrato['tipo'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1($nom_padre, $nom_madre, $nom_est, $grado);
        $this->AddPage();
        $this->pagina2($grado, $valor, $fecha_contrato, $tipo);
        $this->AddPage();
        $this->pagina3($nom_padre, $nom_madre, $ced_padre, $ced_madre, $dir_resi, $tel_resi, $telefono, $correo, $nom_est, $grado);
        $this->AddPage();
        $this->pagina4($nom_padre, $nom_madre, $ced_padre, $ced_madre, $dir_resi, $tel_resi, $telefono, $correo, $nom_est, $grado, $valor, $pagare, $fecha_contrato, $tipo);
    }

    private function pagina1($nom_padre, $nom_madre, $nom_est, $grado)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/contrato/contrato_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // NOMBRE PADRE
        $this->SetXY(18, 79);
        $this->reducirT(50, 5, $nom_padre, 1, 0, 'C');

        // NOMBRE MADRE
        $this->SetXY(72, 79);
        $this->reducirT(55, 5, $nom_madre, 1, 0, 'C');

        // NOMBRE ESTUDIANTE
        $this->SetXY(18, 87);
        $this->reducirT(60, 5, $nom_est, 1, 0, 'C');

        // GRADO
        $this->SetXY(18, 93);
        $this->reducirT(40, 3, $grado, 1, 0, 'C');

        // GRADO
        $this->SetXY(128, 140);
        $this->reducirT(40, 4, $grado, 1, 0, 'C');
    }

    private function pagina2($grado, $valor, $fecha_contrato, $tipo)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/contrato/contrato_page-0002.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // GRADO
        $this->SetXY(120, 35);
        $this->reducirT(30, 4, $grado, 1, 0, 'C');

        // COSTO
        $this->SetXY(160, 39);
        $this->reducirT(30, 4, number_format($valor), 1, 0, 'C');

        $matricula = ($tipo == 1) ? 760000 : 632500;

        // COSTO MATRICULA
        $this->SetXY(38, 48);
        $this->reducirT(40, 4, number_format($matricula), 1, 0, 'C');

        $saldo = ($tipo == 1) ? 3775000 : 3300000;

        // COSTO SALDO
        $this->SetXY(105, 48);
        $this->reducirT(35, 4, number_format($saldo), 1, 0, 'C');

        // DIA
        $this->SetXY(150, 200);
        $this->reducirT(15, 4, date('d', strtotime($fecha_contrato)), 1, 0, 'C');

        // MES
        $this->SetXY(18, 204);
        $this->reducirT(38, 4, strftime("%B", strtotime($fecha_contrato)), 1, 0, 'C');
    }
    private function pagina3($nom_padre, $nom_madre, $ced_padre, $ced_madre, $dir_resi, $tel_resi, $telefono, $correo, $nom_est, $grado)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/contrato/contrato_page-0003.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // NOMBRE PADRE - MADRE
        $this->SetXY(38, 107);
        $this->reducirT(90, 5, $nom_padre . ' - ' . $nom_madre, 1, 0, 'C');

        // CEDULA PADRE - MADRE
        $this->SetXY(40, 112);
        $this->reducirT(90, 5, $ced_padre . ' - ' . $ced_madre, 1, 0, 'C');

        // DIRECCION
        $this->SetXY(58, 117);
        $this->reducirT(35, 5, $dir_resi, 1, 0, 'C');

        // TELEFONO RESIDENCIA
        $this->SetXY(148, 117);
        $this->reducirT(28, 5, $tel_resi, 1, 0, 'C');

        // TELEFONO
        $this->SetXY(40, 122);
        $this->reducirT(40, 5, $telefono, 1, 0, 'C');

        // CORREO ELECTRONICO
        $this->SetXY(120, 122);
        $this->reducirT(50, 5, $correo, 1, 0, 'C');

        // NOMBRE DE ALUMNO
        $this->SetXY(85, 127);
        $this->reducirT(45, 5, $nom_est, 1, 0, 'C');

        // CURSO
        $this->SetXY(155, 127);
        $this->reducirT(25, 5, $grado, 1, 0, 'C');
    }

    private function pagina4($nom_padre, $nom_madre, $ced_padre, $ced_madre, $dir_resi, $tel_resi, $telefono, $correo, $nom_est, $grado, $valor, $pagare, $fecha_contrato, $tipo)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/contrato/contrato_page-0004.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // PAGARE
        $this->SetXY(170, 40);
        $this->reducirT(20, 5, $pagare, 1, 0, 'C');

        // NOSOTROS
        $this->SetXY(45, 59);
        $this->reducirT(95, 5, $nom_padre . ' - ' . $nom_madre, 1, 0, 'C');

        // CEDULA PADRE
        $this->SetXY(18, 69);
        $this->reducirT(50, 5, $ced_padre, 1, 0, 'C');

        // CEDULA MADRE
        $this->SetXY(73, 69);
        $this->reducirT(45, 5, $ced_madre, 1, 0, 'C');

        // EXPEDIDA PADRE
        $this->SetXY(150, 69);
        $this->reducirT(35, 5, '', 1, 0, 'C');

        // EXPEDIDA MADRE
        $this->SetXY(18, 74);
        $this->reducirT(38, 5, '', 1, 0, 'C');

        // ALUMNO
        $this->SetXY(35, 79);
        $this->reducirT(45, 5, $nom_est, 1, 0, 'C');

        // TOTAL LETRAS
        $this->SetXY(18, 93);
        $this->reducirT(86, 5, convertirnumeroletra($valor), 1, 0, 'C');

        // TOTAL NUMEROS
        $this->SetXY(113, 93);
        $this->reducirT(35, 5, number_format($valor), 1, 0, 'C');

        $cuota = ($tipo == 1) ? 760000 : 632500;

        // CUOTAS LETRAS
        $this->SetXY(53, 107);
        $this->reducirT(130, 5, convertirnumeroletra($cuota), 1, 0, 'C');

        // CUOTAS NUMEROS
        $this->SetXY(18, 112);
        $this->reducirT(35, 5, number_format($cuota), 1, 0, 'C');

        // MORA DEUDA
        $this->SetXY(140, 126);
        $this->reducirT(50, 5, '$' . number_format($valor), 1, 0, 'C');

        // DIAS
        $this->SetXY(18, 169);
        $this->reducirT(30, 5, date('d', strtotime($fecha_contrato)), 1, 0, 'C');

        // MES
        $this->SetXY(85, 169);
        $this->reducirT(25, 5, strftime("%B", strtotime($fecha_contrato)), 1, 0, 'C');
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Contrato", true);
$pdf->generar(base64_decode($_GET['estudiante']), base64_decode($_GET['acudiente']));
$pdf->Output('I', 'Contrato.pdf');
