<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 10;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }


    function reducirMultiT($tam, $alt, $string, $tamanio, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->MultiCell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $ali, 0);

        if (!isset($string)) {
            $string = "";
        }
    }

    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(100, 150, 56);
    }

    public function generar($id, $estudiante)
    {
        $instancia = ControlMatricula::singleton_matricula();
        $datos_hoja = $instancia->datosHojaMatriculaControl($id, $estudiante);

        $lug_fecha = $datos_hoja['lug_fecha'];
        $num_folio = $datos_hoja['num_folio'];
        $num_matricula = $datos_hoja['num_matricula'];
        $nom_est = $datos_hoja['nom_est'];
        $lug_fecha_alum = $datos_hoja['lug_fecha_alum'];
        $nombre_padre = $datos_hoja['nombres_padre'];
        $direccion = $datos_hoja['direccion'];
        $telefono = $datos_hoja['telefono'];
        $observacion = $datos_hoja['observacion'];
        $procedencia = $datos_hoja['procedencia'];
        $matricula = $datos_hoja['se_matricula'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1($lug_fecha, $num_folio, $num_matricula, $nom_est, $lug_fecha_alum, $nombre_padre, $direccion, $telefono, $observacion, $procedencia, $matricula);
    }

    private function pagina1($lug_fecha, $num_folio, $num_matricula, $nom_est, $lug_fecha_alum, $nombre_padre, $direccion, $telefono, $observacion, $procedencia, $matricula)
    {
        $this->Image(PUBLIC_PATH . 'img/pdfs/hoja_matricula/hoja_matricula_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // NUMERO DE FOLIO
        $this->SetXY(150, 67);
        $this->reducirT(40, 7, $num_folio, 1, 0, 'C');

        // NUMERO DE MATRICULA
        $this->SetXY(158, 75);
        $this->reducirT(35, 7, $num_matricula, 1, 0, 'C');

        // LUGAR Y FECHA
        $this->SetXY(50, 91);
        $this->reducirT(75, 7, $lug_fecha, 1, 0, 'C');

        // NOMBRE DEL ALUMNO
        $this->SetXY(60, 99);
        $this->reducirT(125, 7, $nom_est, 1, 0, 'C');

        // LUAGR NACIMIENTO
        $this->SetXY(75, 107);
        $this->reducirT(83, 7, $lug_fecha_alum, 1, 0, 'C');

        // EDAD
        $this->SetXY(170, 106);
        $this->reducirT(23, 7, '', 1, 0, 'C');

        // MATRICULA EN
        $this->SetXY(97, 119);
        $this->reducirT(63, 7, $matricula, 1, 0, 'C');

        // PROCEDENCIA
        $this->SetXY(15, 140);
        $this->reducirMultiT(180, 8, $procedencia, 1, 'L');

        // NOMBRE DE LOS PADRES
        $this->SetXY(60, 163);
        $this->reducirT(130, 8, $nombre_padre, 1, 0, 'C');

        // DIRECCION
        $this->SetXY(40, 171);
        $this->reducirT(150, 8, $direccion, 1, 0, 'C');

        // LUGAR
        $this->SetXY(35, 179);
        $this->reducirT(95, 8, '', 1, 0, 'C');

        // TELEFONO
        $this->SetXY(145, 179);
        $this->reducirT(45, 7, $telefono, 1, 0, 'C');

        // OBSERVACIONES
        $this->SetXY(18, 250);
        $this->reducirMultiT(170, 5, $observacion, 1, 'L');
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Hoja de matricula", true);
$pdf->generar(base64_decode($_GET['acudiente']), base64_decode($_GET['estudiante']));
$pdf->Output();
