<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}

require_once LIB_PATH . 'fpdf' . DS . 'fpdf.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

class PDF extends FPDF
{
    const LINE_PRECISION = 5;
    const DEBUG_PDF = 0;
    const BORDER = 0;

    function reducirT($tam, $alt, $string, $tamanio, $salto, $ali)
    {
        if ($string == "--" && !PDF::DEBUG_PDF) {
            return;
        }
        if (!isset($string)) {
            $string = "";
        }
        $tamanio = $tamanio * 10;

        $this->SetFont('Arial', '', $tamanio);  // Set the new font size

        while ($this->GetStringWidth(utf8_decode($string)) > $tam * 1.1) {
            $tamanio--;   // Decrease the variable which holds the font size
            $this->SetFont('Arial', '', $tamanio);  // Set the new font size
        }
        $this->Cell($tam, $alt, trim(utf8_decode($string)), PDF::BORDER, $salto, $ali);

        if (!isset($string)) {
            $string = "";
        }
    }

    private function grilla()
    {
        $this->SetDrawColor(255, 255, 255);
        if (PDF::DEBUG_PDF) {
            for ($i = 0; $i < 300; $i += PDF::LINE_PRECISION) {
                $this->setXY(0, $i);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line(0, $i, 500, $i);
            }
            for ($i = 0; $i < 2000; $i += PDF::LINE_PRECISION) {
                $this->setXY($i, 0);
                $this->Cell(3.5, 2.5, $i, PDF::BORDER, 0, 'C');
                $this->SetLineWidth(0.005);
                $this->Line($i, 0, $i, 2000);
            }
        }
        $this->SetDrawColor(100, 150, 56);
    }

    public function generar($id, $acudiente)
    {
        $instancia = ControlMatricula::singleton_matricula();

        $datos_ficha = $instancia->mostrarFichaControl($acudiente, $id);

        $apellidos = $datos_ficha['apellidos'];
        $nombres = $datos_ficha['nombres'];
        $lug_fecha = $datos_ficha['lug_fecha'];
        $dia = $datos_ficha['dia'];
        $mes = $datos_ficha['mes'];
        $anio = $datos_ficha['anio'];
        $reg_civil = $datos_ficha['reg_civil'];
        $exp_en = $datos_ficha['exp_en'];
        $dir_resi = $datos_ficha['dir_resi'];
        $tel_resi = $datos_ficha['tel_resi'];
        $grdo_mat = $datos_ficha['grdo_mat'];
        $enf_cronica = $datos_ficha['enf_cronica'];
        $espc_enf = $datos_ficha['espc_enf'];
        $fiebre_dolex = $datos_ficha['fiebre_dolex'];
        $emerg_fami = $datos_ficha['emerg_fami'];
        $nombre_fami = $datos_ficha['nombre_fami'];
        $celular_fami = $datos_ficha['celular_fami'];
        $apell_padre = $datos_ficha['apell_padre'];
        $nom_padre = $datos_ficha['nom_padre'];
        $ced_padre = $datos_ficha['ced_padre'];
        $lug_nac_padre = $datos_ficha['lug_nac_padre'];
        $dia_padre = $datos_ficha['dia_padre'];
        $anio_padre = $datos_ficha['anio_padre'];
        $dir_padre = $datos_ficha['dir_padre'];
        $tel_padre = $datos_ficha['tel_padre'];
        $prof_padre = $datos_ficha['prof_padre'];
        $emp_padre = $datos_ficha['emp_padre'];
        $cargo_padre = $datos_ficha['cargo_padre'];
        $dir_oficina_padre = $datos_ficha['dir_oficina_padre'];
        $tel_oficina_padre = $datos_ficha['tel_oficina_padre'];
        $email_padre = $datos_ficha['email_padre'];
        $cel_padre = $datos_ficha['cel_padre'];
        $apell_madre = $datos_ficha['apell_madre'];
        $nom_madre = $datos_ficha['nom_madre'];
        $ced_madre = $datos_ficha['ced_madre'];
        $lug_nac_madre = $datos_ficha['lug_nac_madre'];
        $dia_madre = $datos_ficha['dia_madre'];
        $anio_madre = $datos_ficha['anio_madre'];
        $dir_madre = $datos_ficha['dir_madre'];
        $tel_madre = $datos_ficha['tel_madre'];
        $prof_madre = $datos_ficha['prof_madre'];
        $emp_madre = $datos_ficha['emp_madre'];
        $cargo_madre = $datos_ficha['cargo_madre'];
        $dir_oficina_madre = $datos_ficha['dir_oficina_madre'];
        $tel_oficina_madre = $datos_ficha['tel_oficina_madre'];
        $email_madre = $datos_ficha['email_madre'];
        $cel_madre = $datos_ficha['cel_madre'];
        $nombre_alum = $datos_ficha['nombre_alum'];
        $nivel_alum = $datos_ficha['nivel_alum'];
        $resp_alum = $datos_ficha['resp_alum'];
        $cedula_alum = $datos_ficha['cedula_alum'];
        $exp_alum = $datos_ficha['exp_alum'];
        $lug_nac_alum = $datos_ficha['lug_nac_alum'];
        $dia_alum = $datos_ficha['dia_alum'];
        $mes_alum = $datos_ficha['mes_alum'];
        $anio_alum = $datos_ficha['anio_alum'];
        $parentesco = $datos_ficha['parentesco'];
        $exp_padre = $datos_ficha['exp_padre'];
        $exp_madre = $datos_ficha['exp_madre'];
        $mes_padre = $datos_ficha['mes_padre'];
        $mes_madre = $datos_ficha['mes_madre'];

        $this->SetAutoPageBreak(false);
        $this->AddPage();
        $this->pagina1(
            $apellidos,
            $nombres,
            $lug_fecha,
            $dia,
            $mes,
            $anio,
            $reg_civil,
            $exp_en,
            $dir_resi,
            $tel_resi,
            $grdo_mat,
            $enf_cronica,
            $espc_enf,
            $fiebre_dolex,
            $emerg_fami,
            $nombre_fami,
            $celular_fami,
            $apell_padre,
            $nom_padre,
            $ced_padre,
            $exp_padre,
            $lug_nac_padre,
            $dia_padre,
            $anio_padre,
            $dir_padre,
            $tel_padre,
            $prof_padre,
            $emp_padre,
            $cargo_padre,
            $dir_oficina_padre,
            $tel_oficina_padre,
            $email_padre,
            $cel_padre,
            $apell_madre,
            $nom_madre,
            $ced_madre,
            $exp_madre,
            $lug_nac_madre,
            $dia_madre,
            $anio_madre,
            $dir_madre,
            $tel_madre,
            $prof_madre,
            $emp_madre,
            $cargo_madre,
            $dir_oficina_madre,
            $tel_oficina_madre,
            $email_madre,
            $cel_madre,
            $nombre_alum,
            $nivel_alum,
            $resp_alum,
            $cedula_alum,
            $exp_alum,
            $lug_nac_alum,
            $dia_alum,
            $mes_alum,
            $anio_alum,
            $parentesco,
            $mes_padre,
            $mes_madre
        );
    }

    private function pagina1(
        $apellidos,
        $nombres,
        $lug_fecha,
        $dia,
        $mes,
        $anio,
        $reg_civil,
        $exp_en,
        $dir_resi,
        $tel_resi,
        $grdo_mat,
        $enf_cronica,
        $espc_enf,
        $fiebre_dolex,
        $emerg_fami,
        $nombre_fami,
        $celular_fami,
        $apell_padre,
        $nom_padre,
        $ced_padre,
        $exp_padre,
        $lug_nac_padre,
        $dia_padre,
        $anio_padre,
        $dir_padre,
        $tel_padre,
        $prof_padre,
        $emp_padre,
        $cargo_padre,
        $dir_oficina_padre,
        $tel_oficina_padre,
        $email_padre,
        $cel_padre,
        $apell_madre,
        $nom_madre,
        $ced_madre,
        $exp_madre,
        $lug_nac_madre,
        $dia_madre,
        $anio_madre,
        $dir_madre,
        $tel_madre,
        $prof_madre,
        $emp_madre,
        $cargo_madre,
        $dir_oficina_madre,
        $tel_oficina_madre,
        $email_madre,
        $cel_madre,
        $nombre_alum,
        $nivel_alum,
        $resp_alum,
        $cedula_alum,
        $exp_alum,
        $lug_nac_alum,
        $dia_alum,
        $mes_alum,
        $anio_alum,
        $parentesco,
        $mes_padre,
        $mes_madre
    ) {
        $this->Image(PUBLIC_PATH . 'img/pdfs/ficha/ficha_matricula_page-0001.jpg', '0', '0', '210', '297', 'JPG');
        $this->SetFont('Arial', '', 8);
        $this->grilla();

        // APELLIDOS
        $this->SetXY(37, 58);
        $this->reducirT(65, 5, $apellidos, 1, 0, 'C');

        // NOMBRES
        $this->SetXY(127, 58);
        $this->reducirT(65, 5, $nombres, 1, 0, 'C');

        // LUGAR DE NACIMIENTO
        $this->SetXY(69, 65);
        $this->reducirT(30, 5, $lug_fecha, 1, 0, 'C');

        // DIA
        $this->SetXY(116, 65);
        $this->reducirT(12, 5, $dia, 1, 0, 'C');

        // MES
        $this->SetXY(137, 65);
        $this->reducirT(12, 5, $mes, 1, 0, 'C');

        // AÑO
        $this->SetXY(159, 65);
        $this->reducirT(33, 5, $anio, 1, 0, 'C');

        // REGISTRO CIVIL
        $this->SetXY(55, 72);
        $this->reducirT(43, 5, $reg_civil, 1, 0, 'C');

        // EXPEDIDO EN 
        $this->SetXY(132, 72);
        $this->reducirT(60, 5, $exp_en, 1, 0, 'C');

        // DIRECCION DE RESIDENCIA
        $this->SetXY(55, 78);
        $this->reducirT(43, 5, $dir_resi, 1, 0, 'C');

        // TELEFONO RESIDENCIA
        $this->SetXY(145, 78);
        $this->reducirT(43, 5, $tel_resi, 1, 0, 'C');

        // GRADO EN QUE SE MATRICULA
        $this->SetXY(67, 84);
        $this->reducirT(120, 5, $grdo_mat, 1, 0, 'C');

        if ($enf_cronica == 'si') {
            // SI
            $this->SetXY(95, 90);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($enf_cronica == 'no') {
            // NO
            $this->SetXY(110, 91);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($enf_cronica == 'asma') {
            // ASMA
            $this->SetXY(128, 91);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($enf_cronica == 'alergia') {
            // ALERGIA
            $this->SetXY(152, 91);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        // OTRAS
        $this->SetXY(31, 96);
        $this->reducirT(160, 5, $espc_enf, 1, 0, 'C');

        if ($fiebre_dolex == 'si') {
            // FIEBRE - SI
            $this->SetXY(110, 102);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($fiebre_dolex == 'no') {
            // FIEBRE - NO
            $this->SetXY(125, 102);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($emerg_fami == 'mama') {
            // EMERGENCIA - MAMA
            $this->SetXY(20, 116);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($emerg_fami == 'papa') {
            // EMERGENCIA - PAPA
            $this->SetXY(44, 116);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($emerg_fami == 'otro') {
            // EMERGENCIA - OTRO
            $this->SetXY(67, 116);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        // EMERGENCIA - NOMBRE
        $this->SetXY(100, 116);
        $this->reducirT(58, 5, $nombre_fami, 1, 0, 'C');

        // EMERGENCIA - CEL
        $this->SetXY(165, 116);
        $this->reducirT(28, 5, $celular_fami, 1, 0, 'C');

        // APELLIDO - PADRE
        $this->SetXY(38, 130);
        $this->reducirT(58, 5, $apell_padre, 1, 0, 'C');

        // NOMBRE - PADRE
        $this->SetXY(126, 130);
        $this->reducirT(62, 5, $nom_padre, 1, 0, 'C');

        // CEDULA - PADRE
        $this->SetXY(58, 135);
        $this->reducirT(40, 5, $ced_padre, 1, 0, 'C');

        // EXPEDICION - PADRE
        $this->SetXY(132, 135);
        $this->reducirT(55, 5, $exp_padre, 1, 0, 'C');

        // LUGAR - PADRE
        $this->SetXY(70, 140);
        $this->reducirT(35, 5, $lug_nac_padre, 1, 0, 'C');

        // DIA - PADRE
        $this->SetXY(115, 140);
        $this->reducirT(12, 5, $dia_padre, 1, 0, 'C');

        // MES - PADRE
        $this->SetXY(137, 140);
        $this->reducirT(12, 5, $mes_padre, 1, 0, 'C');

        // AÑO - PADRE
        $this->SetXY(160, 140);
        $this->reducirT(28, 5, $anio_padre, 1, 0, 'C');

        // DIRECCION - PADRE
        $this->SetXY(60, 147);
        $this->reducirT(38, 5, $dir_padre, 1, 0, 'C');

        // TELEFONO - PADRE
        $this->SetXY(125, 147);
        $this->reducirT(20, 5, $tel_padre, 1, 0, 'C');

        // PROFESION - PADRE
        $this->SetXY(165, 147);
        $this->reducirT(27, 5, $prof_padre, 1, 0, 'C');

        // EMPRESA - PADRE
        $this->SetXY(35, 153);
        $this->reducirT(27, 5, $emp_padre, 1, 0, 'C');

        // CARGO - PADRE
        $this->SetXY(73, 153);
        $this->reducirT(27, 5, $cargo_padre, 1, 0, 'C');

        // DIR OFICINA - PADRE
        $this->SetXY(130, 153);
        $this->reducirT(35, 5, $dir_oficina_padre, 1, 0, 'C');

        // TEL OFICINA - PADRE
        $this->SetXY(172, 153);
        $this->reducirT(20, 5, $tel_oficina_padre, 1, 0, 'C');

        // EMAIL - PADRE
        $this->SetXY(32, 160);
        $this->reducirT(65, 5, $email_padre, 1, 0, 'C');

        // CELULAR - PADRE
        $this->SetXY(123, 160);
        $this->reducirT(65, 5, $cel_padre, 1, 0, 'C');

        /*------------------------------------------------*/


        // APELLIDO - MADRE
        $this->SetXY(38, 174);
        $this->reducirT(58, 5, $apell_madre, 1, 0, 'C');

        // NOMBRE - MADRE
        $this->SetXY(126, 174);
        $this->reducirT(62, 5, $nom_madre, 1, 0, 'C');

        // CEDULA - MADRE
        $this->SetXY(58, 179);
        $this->reducirT(40, 5, $ced_madre, 1, 0, 'C');

        // EXPEDICION - MADRE
        $this->SetXY(132, 179);
        $this->reducirT(55, 5, $exp_madre, 1, 0, 'C');

        // LUGAR - MADRE
        $this->SetXY(70, 185);
        $this->reducirT(35, 5, $lug_nac_madre, 1, 0, 'C');

        // DIA - MADRE
        $this->SetXY(115, 185);
        $this->reducirT(12, 5, $dia_madre, 1, 0, 'C');

        // MES - MADRE
        $this->SetXY(137, 185);
        $this->reducirT(12, 5, $mes_madre, 1, 0, 'C');

        // AÑO - MADRE
        $this->SetXY(160, 185);
        $this->reducirT(28, 5, $anio_madre, 1, 0, 'C');

        // DIRECCION - MADRE
        $this->SetXY(60, 192);
        $this->reducirT(38, 5, $dir_madre, 1, 0, 'C');

        // TELEFONO - MADRE
        $this->SetXY(125, 192);
        $this->reducirT(20, 5, $tel_madre, 1, 0, 'C');

        // PROFESION - MADRE
        $this->SetXY(165, 192);
        $this->reducirT(27, 5, $prof_madre, 1, 0, 'C');

        // EMPRESA - MADRE
        $this->SetXY(35, 198);
        $this->reducirT(27, 5, $emp_madre, 1, 0, 'C');

        // CARGO - MADRE
        $this->SetXY(73, 198);
        $this->reducirT(27, 5, $cargo_madre, 1, 0, 'C');

        // DIR OFICINA - MADRE
        $this->SetXY(130, 198);
        $this->reducirT(35, 5, $dir_oficina_madre, 1, 0, 'C');

        // TEL OFICINA - MADRE
        $this->SetXY(172, 198);
        $this->reducirT(20, 5, $tel_oficina_madre, 1, 0, 'C');

        // EMAIL - MADRE
        $this->SetXY(32, 205);
        $this->reducirT(65, 5, $email_madre, 1, 0, 'C');

        // CELULAR - MADRE
        $this->SetXY(123, 205);
        $this->reducirT(65, 5, $cel_madre, 1, 0, 'C');

        /*------------------------------------------------*/

        // NOMBRE DE ALUMNO
        $this->SetXY(58, 227);
        $this->reducirT(65, 5, $nombre_alum, 1, 0, 'C');

        // NIVEL
        $this->SetXY(140, 227);
        $this->reducirT(45, 5, $nivel_alum, 1, 0, 'C');

        // CERTIFICADO
        $this->SetXY(41, 239);
        $this->reducirT(145, 5, $resp_alum, 1, 0, 'C');

        // CEDULA DE CIUDADANIA
        $this->SetXY(58, 244);
        $this->reducirT(35, 5, $cedula_alum, 1, 0, 'C');

        // EXPEDIDA EN
        $this->SetXY(134, 244);
        $this->reducirT(53, 5, $exp_alum, 1, 0, 'C');

        // LUGAR FECHA
        $this->SetXY(70, 250);
        $this->reducirT(35, 5, $lug_nac_alum, 1, 0, 'C');

        // DIA
        $this->SetXY(117, 250);
        $this->reducirT(10, 5, $dia_alum, 1, 0, 'C');

        // MES
        $this->SetXY(138, 250);
        $this->reducirT(10, 5, $mes_alum, 1, 0, 'C');

        // AÑO
        $this->SetXY(160, 250);
        $this->reducirT(35, 5, $anio_alum, 1, 0, 'C');

        if ($parentesco == 'madre') {
            // MADRE
            $this->SetXY(53, 257);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($parentesco == 'padre') {
            // PADRE
            $this->SetXY(75, 257);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }

        if ($parentesco == 'otro') {
            // OTRO
            $this->SetXY(95, 257);
            $this->reducirT(5, 5, 'X', 1, 0, 'C');
        }
    }
}

$pdf = new PDF();
$pdf->SetFont('Arial', '', 8);
$pdf->SetTitle("Autorizacion", true);
$pdf->generar(base64_decode($_GET['estudiante']), base64_decode($_GET['acudiente']));
$pdf->Output();
