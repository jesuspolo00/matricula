<?php
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'ControlSession.php';
$ingreso = ingresoClass::singleton_ingreso();
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12 mt-10">
            <div class="card o-hidden w-responsive border-0 shadow-sm m-auto">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-10 m-auto">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-success-900 mb-4">
                                        <i class="fas fa-prescription"></i>
                                        Validacion
                                    </h1>
                                </div>
                                <form class="user" method="POST" id="form_enviar">
                                    <input type="hidden" name="id_log" value="1">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-control-user numeros user" maxlength="50" id="doc_user" placeholder="Documento" name="documento">
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-user btn-block" id="enviar_datos" value="Validar">
                                    </div>
                                    <hr>
                                    <p class="text-center">¿Ya tienes cuenta? <a href="<?= BASE_URL ?>login">Ingresa aquí</a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['documento'])) {
    $ingreso->validarDocumentoControl();
}
?>