<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$datos_estudiantes = $instancia->mostrarEstudiantesControl();

if ($_SESSION['rol'] != 2) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Volantes de pago
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover table-striped table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No. Solicitud</th>
                                    <th scope="col">Estudiante</th>
                                    <th scope="col">Documento acudiente</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Telefono</th>
                                </tr>
                            </thead>
                            <tbody class="buscar text-lowercase">
                                <?php
                                foreach ($datos_estudiantes as $estudiantes) {
                                    $id_estudiante = $estudiantes['id'];
                                    $nombre = $estudiantes['nom_est'];
                                    $autorizacion = $estudiantes['autorizo'];
                                    $documento = $estudiantes['documento'];
                                    $nombre_completo = $estudiantes['nombre'] . ' ' . $estudiantes['apellido'];
                                    $correo = $estudiantes['correo'];
                                    $telefono = $estudiantes['telefono'];
                                    $id_acudiente = $estudiantes['id_acudiente'];

                                    $volante = $instancia->volanteIdControl($id_estudiante);

                                    if ($volante['id'] == '') {
                                        $span = '';
                                        $descargar = 'd-none';
                                    } else {
                                        $span = 'd-none';
                                        $descargar = '';
                                    }

                                    if ($id_log == $id_acudiente) {
                                ?>
                                        <tr class="text-center">
                                            <td><?= $id_estudiante ?></td>
                                            <td><?= $nombre ?></td>
                                            <td><?= $documento ?></td>
                                            <td><?= $nombre_completo ?></td>
                                            <td><?= $correo ?></td>
                                            <td><?= $telefono ?></td>
                                            <td class="d-none">
                                                <a href="<?= BASE_URL ?>imprimir/contrato?estudiante=<?= base64_encode($id_estudiante) ?>&acudiente=<?= base64_encode($id_acudiente) ?>" target="_blank" class="btn btn-success btn-sm" data-tooltip="tooltip" data-placement="bottom" title="Descargar contrato">
                                                    <i class="fas fa-file-contract"></i>
                                                </a>
                                            </td>
                                            <td>
                                                <span class="<?= $span ?> text-dark"><span class="badge badge-warning">Pendiente</span></span>
                                                <a href="<?= PUBLIC_PATH ?>upload/<?= $volante['nombre'] ?>" download class="btn btn-primary btn-sm <?= $descargar ?>" data-tooltip="tooltip" data-placement="bottom" title="Descargar volante">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                            </td>
                                        </tr>

                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
