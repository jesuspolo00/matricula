<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

if (isset($_GET['estudiante'])) {

    $id_estudiante = base64_decode($_GET['estudiante']);
    $datos_acudiente = $instancia->datosAcudienteControl($id_log);

    if ($datos_acudiente['tipo_user'] == 2) {
        $datos_formato = $instancia->datosPreviosControl($datos_acudiente['documento']);

        $ced_padre = '';
        $nom_padre = $datos_formato['nom_padre'];
        $dir_padre = $datos_formato['dir_padre'];
        $tel_padre = $datos_formato['tel_padre'];
        $ocup_padre = $datos_formato['ocup_padre'];
        $emp_padre = $datos_formato['emp_padre'];
        $cargo_padre = $datos_formato['cargo_padre'];
        $dir_of_padre = '';
        $tel_of_padre = $datos_formato['tel_of_padre'];
        $email_padre = $datos_formato['correo_padre'];
        $cel_padre = $datos_formato['cel_padre'];

        $ced_madre = '';
        $nom_madre = $datos_formato['nom_madre'];
        $dir_madre = $datos_formato['dir_madre'];
        $tel_madre = $datos_formato['tel_madre'];
        $ocup_madre = $datos_formato['ocup_madre'];
        $emp_madre = $datos_formato['emp_madre'];
        $cargo_madre = $datos_formato['cargo_madre'];
        $dir_of_madre = '';
        $tel_of_madre = $datos_formato['tel_of_madre'];
        $email_madre = $datos_formato['correo_madre'];
        $cel_madre = $datos_formato['cel_madre'];

        $nom_est = $datos_formato['nom_est'];
        $grado = $datos_formato['grado_ap'];
    } else {
        $datos_formato = $instancia->datosPadresAntiguosControl($id_log);

        $ced_padre = $datos_formato['ced_padre'];
        $nom_padre = $datos_formato['nom_padre'];
        $dir_padre = '';
        $tel_padre = '';
        $ocup_padre = $datos_formato['prof_padre'];
        $emp_padre = $datos_formato['emp_padre'];
        $cargo_padre = '';
        $dir_of_padre = $datos_formato['dir_emp_padre'];
        $tel_of_padre = '';
        $email_padre = $datos_formato['email_padre'];
        $cel_padre = $datos_formato['cel_padre'];

        $ced_madre = $datos_formato['ced_madre'];
        $nom_madre = $datos_formato['nom_madre'];
        $dir_madre = '';
        $tel_madre = '';
        $ocup_madre = $datos_formato['prof_madre'];
        $emp_madre = $datos_formato['emp_madre'];
        $cargo_madre = '';
        $dir_of_madre = $datos_formato['dir_emp_madre'];
        $tel_of_madre = '';
        $email_madre = $datos_formato['email_madre'];
        $cel_madre = $datos_formato['cel_madre'];

        $nom_est = $datos_formato['estudiante'];
        $grado = '';
    }
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">
                            <a href="<?= BASE_URL ?>inicio" class="text-decoration-none">
                                <i class="fa fa-arrow-left text-primary"></i>
                            </a>
                            &nbsp;
                            Ficha de matricula
                        </h4>
                    </div>
                    <form method="POST">
                        <div class="card-body">
                            <input type="hidden" value="<?= $id_log ?>" name="id_acudiente">
                            <input type="hidden" value="<?= $id_estudiante ?>" name="id_estudiante">
                            <div class="row p-3">
                                <div class="col-lg-12">
                                    <h4 class="text-primary font-weight-bold">DATOS GENERALES</h4>
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Apellidos</label>
                                    <input type="text" name="apellidos" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Nombres</label>
                                    <input type="text" name="nombres" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Lugar y fecha de nacimiento</label>
                                    <input type="text" name="lug_fecha" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dia</label>
                                    <input type="text" name="dia" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Mes</label>
                                    <input type="text" name="mes" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">A&ntilde;o</label>
                                    <input type="text" name="anio" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Registro civil (NUIP)</label>
                                    <input type="text" name="reg_civil" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Expedido en</label>
                                    <input type="text" name="exp_en" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Direccion de residencia</label>
                                    <input type="text" name="dir_resi" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Telefono de residencia</label>
                                    <input type="text" name="tel_resi" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Grado en que se matricula</label>
                                    <input type="text" name="grdo_mat" class="form-control" value="<?= $grado ?>">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Padece de alguna enfermedad cronica</label>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="si" value="si" name="enf_cronica">
                                        <label class="custom-control-label" for="si">Si</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="no" value="no" name="enf_cronica">
                                        <label class="custom-control-label" for="no">No</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="asma" value="asma" name="enf_cronica">
                                        <label class="custom-control-label" for="asma">Asma</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="alergias" value="alergias" name="enf_cronica">
                                        <label class="custom-control-label" for="alergias">Alergias</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="otras" value="otras" name="enf_cronica">
                                        <label class="custom-control-label" for="otras">Otras</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Especifique</label>
                                    <textarea name="espc_enf" class="form-control" id="" cols="30" rows="5"></textarea>
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">En caso de fiebre se puede suministrar DOLEX</label>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="si_fiebre" value="si" name="fiebre_dolex">
                                        <label class="custom-control-label" for="si_fiebre">Si</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="no_fiebre" value="no" name="fiebre_dolex">
                                        <label class="custom-control-label" for="no_fiebre">No</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">En caso de emergencia llamar a</label>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="mama" value="mama" name="emerg_fami">
                                        <label class="custom-control-label" for="mama">Mamá</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="papa" value="papa" name="emerg_fami">
                                        <label class="custom-control-label" for="papa">Papá</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="otro" value="otro" name="emerg_fami">
                                        <label class="custom-control-label" for="otro">Otro</label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Nombre</label>
                                    <input type="text" name="nombre_fami" class="form-control">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Celular</label>
                                    <input type="text" name="celular_fami" class="form-control">
                                </div>

                                <!------------------------------------------------------------------------------------------->

                                <div class="col-lg-12 mt-4">
                                    <h4 class="text-primary font-weight-bold">DATOS DEL PADRE</h4>
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Apellidos</label>
                                    <input type="text" name="apell_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Nombres</label>
                                    <input type="text" name="nom_padre" class="form-control" value="<?= $nom_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Cedula de ciudadanía</label>
                                    <input type="text" name="ced_padre" class="form-control" value="<?= $ced_padre ?>">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Expedida en</label>
                                    <input type="text" name="exp_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Lugar y fecha de nacimiento</label>
                                    <input type="text" name="lug_nac_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dia</label>
                                    <input type="text" name="dia_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Mes</label>
                                    <input type="text" name="mes_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">A&ntilde;o</label>
                                    <input type="text" name="anio_padre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Direccion de residencia</label>
                                    <input type="text" name="dir_padre" class="form-control" value="<?= $dir_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Telefono</label>
                                    <input type="text" name="tel_padre" class="form-control" value="<?= $tel_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Profesion</label>
                                    <input type="text" name="prof_padre" class="form-control" value="<?= $ocup_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Empresa</label>
                                    <input type="text" name="emp_padre" class="form-control" value="<?= $emp_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Cargo</label>
                                    <input type="text" name="cargo_padre" class="form-control" value="<?= $cargo_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dir. Oficina</label>
                                    <input type="text" name="dir_oficina_padre" class="form-control" value="<?= $dir_of_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Telefono</label>
                                    <input type="text" name="tel_oficina_padre" class="form-control" value="<?= $tel_of_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">E-mail</label>
                                    <input type="text" name="email_padre" class="form-control" value="<?= $email_padre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Celular</label>
                                    <input type="text" name="cel_padre" class="form-control" value="<?= $cel_padre ?>">
                                </div>
                                <!------------------------------------------------------------------------------------------->

                                <div class="col-lg-12 mt-4">
                                    <h4 class="text-primary font-weight-bold">DATOS DE LA MADRE</h4>
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Apellidos</label>
                                    <input type="text" name="apell_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Nombres</label>
                                    <input type="text" name="nom_madre" class="form-control" value="<?= $nom_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Cedula de ciudadanía</label>
                                    <input type="text" name="ced_madre" class="form-control" value="<?= $ced_madre ?>">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Expedida en</label>
                                    <input type="text" name="exp_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Lugar y fecha de nacimiento</label>
                                    <input type="text" name="lug_nac_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dia</label>
                                    <input type="text" name="dia_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Mes</label>
                                    <input type="text" name="mes_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">A&ntilde;o</label>
                                    <input type="text" name="anio_madre" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Direccion de residencia</label>
                                    <input type="text" name="dir_madre" class="form-control" value="<?= $dir_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Telefono</label>
                                    <input type="text" name="tel_madre" class="form-control" value="<?= $tel_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Profesion</label>
                                    <input type="text" name="prof_madre" class="form-control" value="<?= $ocup_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Empresa</label>
                                    <input type="text" name="emp_madre" class="form-control" value="<?= $emp_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Cargo</label>
                                    <input type="text" name="cargo_madre" class="form-control" value="<?= $cargo_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dir. Oficina</label>
                                    <input type="text" name="dir_oficina_madre" class="form-control" value="<?= $dir_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Telefono</label>
                                    <input type="text" name="tel_oficina_madre" class="form-control" value="<?= $tel_of_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">E-mail</label>
                                    <input type="text" name="email_madre" class="form-control" value="<?= $email_madre ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Celular</label>
                                    <input type="text" name="cel_madre" class="form-control" value="<?= $cel_madre ?>">
                                </div>
                                <!------------------------------------------------------------------------------------------->

                                <div class="col-lg-12 mt-4">
                                    <h4 class="text-primary font-weight-bold">INFORMACION PARA USO INTERNO</h4>
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Nombre del alumno</label>
                                    <input type="text" name="nombre_alum" class="form-control" value="<?= $nom_est ?>">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Nivel</label>
                                    <input type="text" name="nivel_alum" class="form-control" value="<?= $grado ?>">
                                </div>
                                <div class="form-group col-lg-12 mt-2">
                                    <label class="font-weight-bold">Nombre completo de la persona que aparece como responsable de los pagos (Para la elaboración de certificados)</label>
                                    <input type="text" name="resp_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Cedula de ciudadania</label>
                                    <input type="text" name="cedula_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Expedida en</label>
                                    <input type="text" name="exp_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Lugar y fecha de nacimiento</label>
                                    <input type="text" name="lug_nac_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Dia</label>
                                    <input type="text" name="dia_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">Mes</label>
                                    <input type="text" name="mes_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-4 mt-2">
                                    <label class="font-weight-bold">A&ntilde;o</label>
                                    <input type="text" name="anio_alum" class="form-control">
                                </div>
                                <div class="form-group col-lg-6 mt-2">
                                    <label class="font-weight-bold">Parentesco</label>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="mama_parent" value="madre" name="parentesco">
                                        <label class="custom-control-label" for="mama_parent">Mamá</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="papa_parent" value="padre" name="parentesco">
                                        <label class="custom-control-label" for="papa_parent">Papá</label>
                                    </div>
                                    <!-- Default unchecked -->
                                    <div class="custom-control custom-radio">
                                        <input type="radio" required class="custom-control-input" id="otro_parent" value="otro" name="parentesco">
                                        <label class="custom-control-label" for="otro_parent">Otro</label>
                                    </div>
                                </div>
                                <div class="form-gorup col-lg-12">
                                    <button type="submit" class="btn btn-success btn-sm float-right">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Guardar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
    include_once VISTA_PATH . 'script_and_final.php';

    if (isset($_POST['id_acudiente'])) {
        $instancia->guardarFormatoControl();
    }
}
