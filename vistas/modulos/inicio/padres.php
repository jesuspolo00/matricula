<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'matricula' . DS . 'ControlMatricula.php';

$instancia = ControlMatricula::singleton_matricula();

$datos_estudiante = $instancia->mostrarDatosEstudianteControl($id_log);
$documentos =  $instancia->mostrarDocumentosControl($id_log, $datos_estudiante['id']);
$ficha_matricula = $instancia->mostrarFichaControl($id_log, $datos_estudiante['id']);

if ($datos_estudiante['id'] != '') {
    $color = 'bg-success';
    $nombre = $datos_estudiante['nom_est'];
    $disabled = 'disabled';
    $text = 'text-white';
    $ver = '';
} else {
    $color = '';
    $nombre = '';
    $disabled = '';
    $text = '';
    $ver = 'd-none';
}

if ($documentos[0]['id'] == '') {
    $color_doc = '';
    $disabled_doc = '';
    $text_doc = '';
} else {
    $color_doc = 'bg-success';
    $disabled_doc = 'disabled';
    $text_doc = 'text-white';
}

if ($ficha_matricula['id'] == '') {
    $color_ficha = '';
    $disabled_ficha = '';
    $text_ficha = '';
    $href = BASE_URL . 'inicio/formato?estudiante=' . base64_encode($datos_estudiante['id']);
} else {
    $color_ficha = 'bg-success';
    $disabled_ficha = 'disabled';
    $text_ficha = 'text-white';
    $href = '#';
}

if ($ficha_matricula['id'] != '' && $documentos[0]['id'] != '' && $datos_estudiante['id'] != '') {
    $disabled_final = '';
    $color_btn = 'btn-success';
} else {
    $disabled_final = 'disabled';
    $color_btn = 'btn-secondary';
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-primary">
                        Requisitos para matrícula
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header <?= $color ?>" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link <?= $text ?>" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                - Datos del estudiante
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form method="POST">
                                                <input type="hidden" value="<?= $id_log ?>" name="id_acudiente">
                                                <div class="form-group col-lg-6">
                                                    <label>Nombre del estudiante</label>
                                                    <input type="text" class="form-control letras" name="nom_est" <?= $disabled ?> value="<?= $nombre ?>">
                                                </div>
                                                <div class="col-lg-12 text-right">
                                                    <button class="btn btn-success btn-sm" <?= $disabled ?>>
                                                        <i class="fa fa-save"></i>
                                                        &nbsp;
                                                        Guardar
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-2 <?= $ver ?>">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header <?= $color_ficha ?>" id="headingOne">
                                        <h5 class="mb-0">
                                            <a class="btn btn-link <?= $text_ficha ?>" href="<?= $href ?>">
                                                - Ficha de matricula
                                            </a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 mt-2 <?= $ver ?>">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header <?= $color_doc ?>" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link <?= $text_doc ?>" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                                - Documentos
                                            </button>
                                            <span class="text-danger h6">Es requisito entregarlos completos, de lo contrario no se podrá oficializar la matrícula.</span>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            <form method="POST" enctype="multipart/form-data">
                                                <div class="row">
                                                    <input type="hidden" value="<?= $id_log ?>" name="id_acudiente_arch">
                                                    <input type="hidden" value="<?= $datos_estudiante['id'] ?>" name="id_estudiante">
                                                    <div class="form-group col-lg-6">
                                                        <label>Primera foto recientes de 3 x 4cm, marcadas con el nombre del alumno. <span class="text-danger">*</span></label>
                                                        <input id="file" type="file" class="file" name="foto_est" accept=".png,.jpg,.jpeg" data-preview-file-type="any" <?= $disabled_doc ?> >
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Segunda foto recientes de 3 x 4cm, marcadas con el nombre del alumno. <span class="text-danger">*</span></label>
                                                        <input id="file" type="file" class="file" name="foto_est2" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?> >
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Volante de consignación. <span class="text-danger">*</span></label>
                                                        <input id="file" type="file" class="file" name="volante" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?> >
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Copia del registro civil . <span class="text-danger">(Solo estudiante nuevo)</span></label>
                                                        <input id="file" type="file" class="file" name="reg_civil" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?>>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Certificado médico de salud.</label>
                                                        <input id="file" type="file" class="file" name="cert_salud" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?> >
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Examen visual y auditivo.</label>
                                                        <input id="file" type="file" class="file" name="ex_visual" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?>>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Copia del esquema de vacunación.</label>
                                                        <input id="file" type="file" class="file" name="esq_vacuna" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?>>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Copia del carnet o certificado de afiliación de la EPS. <span class="text-danger">*</span></label>
                                                        <input id="file" type="file" class="file" name="cert_eps" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?> >
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>En caso de padres separados, fotocopia de documento de Custodia Legal. <span class="text-danger">(Solo estudiante nuevo)</span></label>
                                                        <input id="file" type="file" class="file" name="doc_custodia" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?>>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label>Paz y Salvo del colegio anterior. <span class="text-danger">(Solo estudiante nuevo)</span></label>
                                                        <input id="file" type="file" class="file" name="paz_salvo" accept=".png,.jpg,.jpeg,.pdf" data-preview-file-type="any" <?= $disabled_doc ?>>
                                                    </div>
                                                    <div class="col-lg-12 text-right">
                                                        <button class="btn btn-success btn-sm" <?= $disabled_doc ?>>
                                                            <i class="fa fa-save"></i>
                                                            &nbsp;
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12 mt-4">
                            <p class="text-danger ml-2">
                                <span class="font-weight-bold">IMPORTANTE:</span>
                                Se requiere de la presencia de los Padres de familia para firmar la matrícula, de lo contrario no será oficializada.
                                <br>
                                No se permiten pagos parciales.
                                <br>
                                Los cheques devueltos dan por nulo la matrícula.
                            </p>
                            <form method="POST">
                                <input type="hidden" name="id_acudiente" value="<?= $id_log ?>">
                                <input type="hidden" name="id_estudiante" value="<?= $datos_estudiante['id'] ?>">
                                <div class="form-group col-lg-12">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="autorizar" value="si" name="autorizar">
                                        <label class="custom-control-label font-weight-bold" for="autorizar">Autorizo al Jardín Infantil Play and Learn a darle uso pedagógico a las imágenes (fotos) realizadas durante la jornada escolar publicadas en La página web del Jardín:
                                            <a href="http://playandlearn.edu.co/" target="_blank">www.playandlearn.edu.co</a> e Instagram:
                                            <a href="https://www.instagram.com/jardinplayandlearn" target="_blank">@jardinplayandlearn</a></label>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 text-right">
                                    <a href="<?= BASE_URL ?>salir" class="btn btn-primary btn-sm">
                                        <i class="fa fa-save"></i>
                                        &nbsp;
                                        Guardar avance
                                    </a>
                                    &nbsp;
                                    <button type="submit" class="btn <?= $color_btn ?> btn-sm" <?= $disabled_final ?>>
                                        <i class="fa fa-check"></i>
                                        &nbsp;
                                        Finalizar
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['nom_est'])) {
    $instancia->agregarEstudianteControl();
}

if (isset($_POST['id_acudiente_arch'])) {
    $instancia->guardarDocumentosControl();
}

if (isset($_POST['id_estudiante'])) {
    $instancia->finalizarMatriculaControl();
}
