<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';

$permisos = $instancia_permiso->permisosUsuarioControl(2, 1, 1, $id_log);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary">Configuracion</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>usuarios/index">
                                <div class="card border-left-success shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Usuarios</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-users fa-2x text-success"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 3, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>acudiente/index">
                                <div class="card border-left-info shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Acudientes</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-user-friends fa-2x text-info"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>matricula/index">
                                <div class="card border-left-danger shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Matriculas</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-pencil-ruler fa-2x text-danger"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 5, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>matricula/volantes">
                                <div class="card border-left-secondary shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Volantes de pago</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-money-check fa-2x text-secondary"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php }
                        $permisos = $instancia_permiso->permisosUsuarioControl(2, 6, 1, $id_log);
                        if ($permisos) {
                        ?>
                            <a class="col-md-4 mb-4 text-decoration-none" href="<?= BASE_URL ?>contratos/index">
                                <div class="card border-left-primary shadow-sm h-100 py-2">
                                    <div class="card-body">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col mr-2">
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">Contratos</div>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-file-contract fa-2x text-primary"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>