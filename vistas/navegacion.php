<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1) {
  $er = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_permiso = ControlPermisos::singleton_permisos();
$id_log = $_SESSION['id'];
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= BASE_URL ?>inicio">
    <div class="sidebar-brand-icon">
      <i class="fas fa-prescription text-primary"></i>
    </div>
    <div class="sidebar-brand-text mx-3 text-primary mt-3">
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?= BASE_URL ?>inicio">
      <i class="fas fa-home text-primary"></i>
      <span class="text-muted">Inicio</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider bg-gray">

    <?php
    $permisos = $instancia_permiso->permisosUsuarioControl(2, 2, 1, $id_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= BASE_URL ?>usuarios/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users text-primary"></i>
          <span class="text-muted">Usuarios</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosUsuarioControl(2, 3, 1, $id_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= BASE_URL ?>acudiente/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-user-friends text-primary"></i>
          <span class="text-muted">Acudientes</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosUsuarioControl(2, 4, 1, $id_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= BASE_URL ?>matricula/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-pencil-ruler text-primary"></i>
          <span class="text-muted">Matriculas</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosUsuarioControl(2, 5, 1, $id_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= BASE_URL ?>matricula/volantes" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-money-check text-primary"></i>
          <span class="text-muted">Volantes de pago</span>
        </a>
      </li>

    <?php }
    $permisos = $instancia_permiso->permisosUsuarioControl(2, 6, 1, $id_log);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= BASE_URL ?>contratos/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-file-contract text-primary"></i>
          <span class="text-muted">Contratos</span>
        </a>
      </li>
      <?php
    }
  //$permisos = $instancia_permiso->permisosUsuarioControl(2, 5, 1, $id_log);
    if ($_SESSION['rol'] == 2) {
      ?>
    <!-- <li class="nav-item">
      <a class="nav-link collapsed" href="<?= BASE_URL ?>inicio/volantes" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-money-check text-primary"></i>
        <span class="text-muted">Volantes de pago</span>
      </a>
    </li> -->
  <?php } ?>
  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block bg-gray">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars text-primary"></i>
      </button>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido'] ?></span>
            <img class="img-profile rounded-circle" src="<?= PUBLIC_PATH ?>img/user.svg">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="<?= BASE_URL ?>perfil/index">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
              Perfil
            </a>
            <?php
            $permisos = $instancia_permiso->permisosUsuarioControl(2, 1, 1, $id_log);
            if ($permisos) {
              ?>
              <a class="dropdown-item" href="<?= BASE_URL ?>configuracion/index">
                <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
                Configuracion
              </a>
              <?php
            }
            ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= BASE_URL ?>salir">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Cerrar sesion
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <?php
      include_once VISTA_PATH . 'script_and_final.php';
      ?>