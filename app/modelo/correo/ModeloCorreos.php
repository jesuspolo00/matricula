<?php
require_once LIB_PATH . 'PHPMailer/PHPMailerAutoload.php';
require_once MODELO_PATH . 'configMail.php';

class Correo extends PHPMailer
{

    public static function enviarCorreoModel($datos)
    {

        $mail = new Correo();

        $mail->IsSMTP();
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = SMTP;
        $mail->Host       = SERVER;
        $mail->Port       = PORT;

        $mail->Username = USER;
        $mail->Password = PASS;

        $mail->From     = USER;
        $mail->FromName = NOMBRE;

        $mail->Subject = $datos['asunto'];
        $mail->AddAddress($datos['correo'], $datos['user']);

        $mail->MsgHTML($datos['mensaje']);

        if (!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }
}
