<?php

require_once 'conexion.php';

class IngresoModel extends conexion
{


    public static function verificarUser($nick)
    {
        $cnx = conexion::singleton_conexion();
        $cmd = "SELECT * FROM usuarios WHERE user = '" . $nick . "'";
        try {
            $preparado = $cnx->preparar($cmd);
            if ($preparado->execute()) {
                if ($preparado->rowCount() >= 1) {
                    return $preparado->fetch();
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public static function validarDocumentoModel($nick)
    {
        $cnx = conexion::singleton_conexion();
        $cmd = "SELECT * FROM usuarios WHERE documento = :n AND perfil = 2";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindValue(':n', $nick);
            if ($preparado->execute()) {
                if ($preparado->rowCount() >= 1) {
                    return $preparado->fetch();
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public static function agregarAcudienteModel($datos)
    {
        $tabla = 'usuarios';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, user, pass, perfil, user_log) 
        VALUES (:d,:n,:a,:c,:t,:us,:p,:pr,:ul);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':us', $datos['usuario']);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':pr', $datos['perfil']);
            $preparado->bindParam(':ul', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
