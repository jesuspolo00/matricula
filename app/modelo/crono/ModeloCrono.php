<?php
require_once MODELO_PATH . 'conexion_crono.php';

class ModeloCrono extends conexion_crono
{
    public static function datosPreviosModel($id)
    {
        $tabla  = 'solicitud_ingreso';
        $cnx    = conexion_crono::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . "
        WHERE id_acudiente IN(SELECT u.id_user FROM usuarios u WHERE u.documento = :d) ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':d', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
