<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloMatricula extends conexion
{

    public static function agregarEstudianteModel($datos)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_acudiente, nom_est)
        VALUES (:id,:n);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_acudiente']);
            $preparado->bindParam(':n', $datos['nom_est']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosEstudianteModel($id)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND activo = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function detallePrematriculaModel($id)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        e.*,
        e.id AS solicitud,
        IF(e.id IS NULL, 'no', 'si') AS registro,
        (SELECT IF(f.id IS NULL, 'no', 'si') FROM ficha_matricula f WHERE f.id_estudiante = e.id ORDER BY f.id DESC LIMIT 1) AS ficha_matricula,
        (SELECT IF(d.id IS NULL, 'no', 'si') FROM documentos d WHERE d.id_estudiante = e.id ORDER BY id DESC LIMIT 1) AS documentos
        FROM usuarios u
        LEFT JOIN estudiante e ON u.id_user = e.id_acudiente
        WHERE u.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function volanteIdModel($id)
    {
        $tabla  = 'volantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_estudiante = :est ORDER BY id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':est', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHojaMatriculaModel($id, $estudiante)
    {
        $tabla  = 'hoja_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_acudiente = :id AND id_estudiante = :est ORDER BY id DESC LIMIT 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':est', $estudiante);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosEstudianteIdModel($id)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT es.*, u.* FROM " . $tabla . " es
        INNER JOIN usuarios u ON es.id_acudiente = u.id_user
        WHERE es.activo = 0 AND es.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEstudiantesModel()
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT es.*, u.* FROM " . $tabla . " es
        INNER JOIN usuarios u ON es.id_acudiente = u.id_user
        WHERE es.activo = 0;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosPadresAntiguosModel($id)
    {
        $tabla  = 'detalle_padre';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT d.*,
        (SELECT e.nom_est FROM estudiante e WHERE e.id_acudiente = d.id_acudiente AND e.activo = 1) AS estudiante
        FROM detalle_padre d WHERE d.id_acudiente = :id ORDER BY d.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function finalizarMatriculaModel($datos)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET autorizo = :at, activo = 0 WHERE id_acudiente = :id AND id = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':at', $datos['autorizar']);
            $preparado->bindParam(':id', $datos['id_acudiente']);
            $preparado->bindParam(':ids', $datos['id_estudiante']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarFichaModel($id, $estudiante)
    {
        $tabla  = 'estudiante';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM ficha_matricula WHERE id_acudiente = :id AND id_estudiante = :ids ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':ids', $estudiante);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDocumentosModel($id, $estudiante)
    {
        $tabla  = 'documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT d.*,
        (SELECT t.nombre FROM tipo_documento t WHERE t.id = d.tipo_doc) AS tipo
        FROM " . $tabla . " d WHERE d.id_acudiente = :id AND d.id_estudiante = :est;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            $preparado->bindValue(':est', $estudiante);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarDocumentoModel($datos)
    {
        $tabla  = 'documentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_acudiente, tipo_doc, id_estudiante) VALUES (:n,:id,:td,:ide);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_acudiente']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':td', $datos['tipo_doc']);
            $preparado->bindParam(':ide', $datos['id_estudiante']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarFormatoModel($datos)
    {
        var_dump($datos);
        $tabla  = 'ficha_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (
        id_acudiente,
        apellidos,
        nombres,
        lug_fecha,
        dia,
        mes,
        anio,
        reg_civil,
        exp_en,
        dir_resi,
        tel_resi,
        grdo_mat,
        enf_cronica,
        espc_enf,
        fiebre_dolex,
        emerg_fami,
        nombre_fami,
        celular_fami,
        apell_padre,
        nom_padre,
        ced_padre,
        lug_nac_padre,
        dia_padre,
        anio_padre,
        dir_padre,
        tel_padre,
        prof_padre,
        emp_padre,
        cargo_padre,
        dir_oficina_padre,
        tel_oficina_padre,
        email_padre,
        cel_padre,
        apell_madre,
        nom_madre,
        ced_madre,
        lug_nac_madre,
        dia_madre,
        anio_madre,
        dir_madre,
        tel_madre,
        prof_madre,
        emp_madre,
        cargo_madre,
        dir_oficina_madre,
        tel_oficina_madre,
        email_madre,
        cel_madre,
        nombre_alum,
        nivel_alum,
        resp_alum,
        cedula_alum,
        exp_alum,
        lug_nac_alum,
        dia_alum,
        mes_alum,
        anio_alum,
        parentesco,
        confirmado,
        id_estudiante,
        exp_padre,
        exp_madre,
        mes_padre,
        mes_madre
        )
        VALUES
        (
        '" . $datos['id_acudiente'] . "',
        '" . $datos['apellidos'] . "',
        '" . $datos['nombres'] . "',
        '" . $datos['lug_fecha'] . "',
        '" . $datos['dia'] . "',
        '" . $datos['mes'] . "',
        '" . $datos['anio'] . "',
        '" . $datos['reg_civil'] . "',
        '" . $datos['exp_en'] . "',
        '" . $datos['dir_resi'] . "',
        '" . $datos['tel_resi'] . "',
        '" . $datos['grdo_mat'] . "',
        '" . $datos['enf_cronica'] . "',
        '" . $datos['espc_enf'] . "',
        '" . $datos['fiebre_dolex'] . "',
        '" . $datos['emerg_fami'] . "',
        '" . $datos['nombre_fami'] . "',
        '" . $datos['celular_fami'] . "',
        '" . $datos['apell_padre'] . "',
        '" . $datos['nom_padre'] . "',
        '" . $datos['ced_padre'] . "',
        '" . $datos['lug_nac_padre'] . "',
        '" . $datos['dia_padre'] . "',
        '" . $datos['anio_padre'] . "',
        '" . $datos['dir_padre'] . "',
        '" . $datos['tel_padre'] . "',
        '" . $datos['prof_padre'] . "',
        '" . $datos['emp_padre'] . "',
        '" . $datos['cargo_padre'] . "',
        '" . $datos['dir_oficina_padre'] . "',
        '" . $datos['tel_oficina_padre'] . "',
        '" . $datos['email_padre'] . "',
        '" . $datos['cel_padre'] . "',
        '" . $datos['apell_madre'] . "',
        '" . $datos['nom_madre'] . "',
        '" . $datos['ced_madre'] . "',
        '" . $datos['lug_nac_madre'] . "',
        '" . $datos['dia_madre'] . "',
        '" . $datos['anio_madre'] . "',
        '" . $datos['dir_madre'] . "',
        '" . $datos['tel_madre'] . "',
        '" . $datos['prof_madre'] . "',
        '" . $datos['emp_madre'] . "',
        '" . $datos['cargo_madre'] . "',
        '" . $datos['dir_oficina_madre'] . "',
        '" . $datos['tel_oficina_madre'] . "',
        '" . $datos['email_madre'] . "',
        '" . $datos['cel_madre'] . "',
        '" . $datos['nombre_alum'] . "',
        '" . $datos['nivel_alum'] . "',
        '" . $datos['resp_alum'] . "',
        '" . $datos['cedula_alum'] . "',
        '" . $datos['exp_alum'] . "',
        '" . $datos['lug_nac_alum'] . "',
        '" . $datos['dia_alum'] . "',
        '" . $datos['mes_alum'] . "',
        '" . $datos['anio_alum'] . "',
        '" . $datos['parentesco'] . "',
        '" . $datos['confirmado'] . "',
        '" . $datos['id_estudiante'] . "',
        '" . $datos['exp_padre'] . "',
        '" . $datos['exp_madre'] . "',
        '" . $datos['mes_padre'] . "',
        '" . $datos['mes_madre'] . "'
        );
        ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosAcudienteModel($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM usuarios WHERE id_user = :d;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':d', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosContratoModel($estudiante, $acudiente)
    {
        $tabla  = 'ficha_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        (SELECT c.id FROM contrato c WHERE c.id_estudiante = f.id_estudiante AND c.id_acudiente = f.id_acudiente) AS id,
        f.nom_padre,
        f.nom_madre,
        (SELECT e.nom_est FROM estudiante e WHERE e.id = f.id_estudiante) AS estudiante,
        (SELECT g.se_matricula FROM hoja_matricula g WHERE g.id_estudiante = f.id_estudiante AND g.id_acudiente = f.id_acudiente) AS grado,
        (SELECT c.valor FROM contrato c WHERE c.id_estudiante = f.id_estudiante AND c.id_acudiente = f.id_acudiente) AS valor,
        (SELECT c.pagare FROM contrato c WHERE c.id_estudiante = f.id_estudiante AND c.id_acudiente = f.id_acudiente) AS pagare,
        (SELECT c.fechareg FROM contrato c WHERE c.id_estudiante = f.id_estudiante AND c.id_acudiente = f.id_acudiente) AS fecha_contrato,
        (SELECT c.tipo FROM contrato c WHERE c.id_estudiante = f.id_estudiante AND c.id_acudiente = f.id_acudiente) AS tipo,
        f.ced_padre,
        f.ced_madre,
        f.dir_resi,
        f.tel_resi,
        IF(f.tel_padre IS NULL, f.tel_madre, f.tel_padre) AS telefono,
        IF(f.email_padre IS NULL, f.email_madre, f.email_padre) AS correo
        FROM " . $tabla . " f WHERE f.id_estudiante = :idest AND f.id_acudiente = :idac;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idac', $acudiente);
            $preparado->bindValue(':idest', $estudiante);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function datosHojaMatriculaModel($id, $estudiante)
    {
        $tabla  = 'hoja_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT h.*,
        (SELECT e.nom_est FROM estudiante e WHERE e.id = h.id_estudiante) AS nom_est,
        (SELECT CONCAT(f.lug_nac_alum, ' ', f.dia_alum, '-',f.mes_alum,'-',f.anio_alum) FROM ficha_matricula f WHERE f.id_acudiente = h.id_acudiente AND f.id_estudiante = h.id_estudiante ORDER BY f.id DESC LIMIT 1) lug_fecha_alum,
        (SELECT CONCAT(f.nom_padre ,' - ' , f.nom_madre) FROM ficha_matricula f WHERE f.id_acudiente = h.id_acudiente AND f.id_estudiante = h.id_estudiante ORDER BY f.id DESC LIMIT 1) nombres_padre,
        (SELECT IF(f.dir_padre IS NULL, f.dir_madre, f.dir_padre) FROM ficha_matricula f WHERE f.id_acudiente = h.id_acudiente AND f.id_estudiante = h.id_estudiante ORDER BY f.id DESC LIMIT 1) direccion,
        (SELECT IF(f.tel_padre IS NULL, f.tel_madre, f.tel_padre) FROM ficha_matricula f WHERE f.id_acudiente = h.id_acudiente AND f.id_estudiante = h.id_estudiante ORDER BY f.id DESC LIMIT 1) telefono
        FROM " . $tabla . " h
        WHERE h.id_acudiente = :idc AND h.id_estudiante = :id ORDER BY h.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':idc', $id);
            $preparado->bindValue(':id', $estudiante);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function generarHojaMatriculaModel($datos)
    {
        $tabla  = 'hoja_matricula';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_acudiente, id_estudiante, num_folio, num_matricula, lug_fecha, id_user, procedencia, observacion, se_matricula)
        VALUES (:iac, :iest, :nf, :nm, :lf, :id, :p, :ob, :m);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':iac', $datos['id_acudiente']);
            $preparado->bindParam(':iest', $datos['id_estudiante']);
            $preparado->bindParam(':nf', $datos['num_folio']);
            $preparado->bindParam(':nm', $datos['num_matricula']);
            $preparado->bindParam(':lf', $datos['lug_fecha']);
            $preparado->bindParam(':id', $datos['id_log']);
            $preparado->bindParam(':p', $datos['procedencia']);
            $preparado->bindParam(':ob', $datos['observacion']);
            $preparado->bindParam(':m', $datos['se_matricula']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function subirVolanteModel($datos)
    {
        $tabla  = 'volantes';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_estudiante, id_acudiente, nombre, id_log) VALUES (:iest, :iac, :n, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':iac', $datos['id_acudiente']);
            $preparado->bindParam(':iest', $datos['id_estudiante']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function generarContratoModel($datos)
    {
        $tabla  = 'contrato';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_estudiante, id_acudiente, valor, pagare, id_log, tipo) VALUES (:est, :acu, :v, :p, :idl, :tp);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':acu', $datos['id_acudiente']);
            $preparado->bindParam(':est', $datos['id_estudiante']);
            $preparado->bindParam(':v', $datos['valor']);
            $preparado->bindParam(':p', $datos['pagare']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':tp', $datos['tipo']);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
