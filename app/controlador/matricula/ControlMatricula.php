<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'matricula' . DS . 'ModeloMatricula.php';
require_once MODELO_PATH . 'crono' . DS . 'ModeloCrono.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';

class ControlMatricula
{

    private static $instancia;

    public static function singleton_matricula()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function datosContratoControl($estudiante, $acudiente)
    {
        $mostrar = ModeloMatricula::datosContratoModel($estudiante, $acudiente);
        return $mostrar;
    }

    public function volanteIdControl($estudiante)
    {
        $mostrar = ModeloMatricula::volanteIdModel($estudiante);
        return $mostrar;
    }


    public function detallePrematriculaControl($id)
    {
        $mostrar = ModeloMatricula::detallePrematriculaModel($id);
        return $mostrar;
    }


    public function mostrarHojaMatriculaControl($id, $estudiante)
    {
        $mostrar = ModeloMatricula::mostrarHojaMatriculaModel($id, $estudiante);
        return $mostrar;
    }

    public function datosHojaMatriculaControl($id, $estudiante)
    {
        $mostrar = ModeloMatricula::datosHojaMatriculaModel($id, $estudiante);
        return $mostrar;
    }

    public function datosPreviosControl($id)
    {
        $mostrar = ModeloCrono::datosPreviosModel($id);
        return $mostrar;
    }

    public function datosPadresAntiguosControl($id)
    {
        $mostrar = ModeloMatricula::datosPadresAntiguosModel($id);
        return $mostrar;
    }

    public function datosAcudienteControl($id)
    {
        $mostrar = ModeloMatricula::datosAcudienteModel($id);
        return $mostrar;
    }

    public function mostrarDatosEstudianteIdControl($id)
    {
        $mostrar = ModeloMatricula::mostrarDatosEstudianteIdModel($id);
        return $mostrar;
    }


    public function mostrarEstudiantesControl()
    {
        $mostrar = ModeloMatricula::mostrarEstudiantesModel();
        return $mostrar;
    }


    public function mostrarDatosEstudianteControl($id)
    {
        $mostrar = ModeloMatricula::mostrarDatosEstudianteModel($id);
        return $mostrar;
    }


    public function mostrarDocumentosControl($id, $estudiante)
    {
        $mostrar = ModeloMatricula::mostrarDocumentosModel($id, $estudiante);
        return $mostrar;
    }


    public function mostrarFichaControl($id, $estudiante)
    {
        $mostrar = ModeloMatricula::mostrarFichaModel($id, $estudiante);
        return $mostrar;
    }


    public function finalizarMatriculaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {
            $datos = array(
                'id_acudiente' => $_POST['id_acudiente'],
                'id_estudiante' => $_POST['id_estudiante'],
                'autorizar' => $_POST['autorizar']
            );

            $guardar = ModeloMatricula::finalizarMatriculaModel($datos);

            if ($guardar == TRUE) {

                $datos_acudiente = $this->datosAcudienteControl($_POST['id_acudiente']);

                $mensaje = '<p>El siguiente correo es para informar que el usuario <span style="font-weight: bold;">' . $datos_acudiente['nombre'] . ' ' . $datos_acudiente['apellido'] . '</span> ha completado el proceso de matricula.</p>';

                $datos_correo = array(
                    'asunto' => 'Proceso de matricula',
                    'correo' => 'angel.vargas@royalschool.edu.co',
                    //'correo' => $datos_acudiente['correo'],
                    'user' => $datos_acudiente['nombre'] . ' ' . $datos_acudiente['apellido'],
                    'mensaje' => $mensaje
                );

                $enviar = Correo::enviarCorreoModel($datos_correo);

                echo '
                <script>
                ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("../salir");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }



    public function generarHojaMatriculaControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {
            $datos = array(
                'id_acudiente' => $_POST['id_acudiente'],
                'id_estudiante' => $_POST['id_estudiante'],
                'num_folio' => $_POST['num_folios'],
                'num_matricula' => $_POST['num_mat'],
                'lug_fecha' => date('d-m-Y') . ' Barranquilla - Atlántico',
                'id_log' => $_POST['id_log'],
                'procedencia' => $_POST['procedencia'],
                'observacion' => $_POST['observacion'],
                'se_matricula' => $_POST['matricula']
            );

            $guardar = ModeloMatricula::generarHojaMatriculaModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Generado correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }



    public function guardarFormatoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {
            $id_acudiente = $_POST['id_acudiente'];
            $apellidos = $_POST['apellidos'];
            $nombres = $_POST['nombres'];
            $lug_fecha = $_POST['lug_fecha'];
            $dia = $_POST['dia'];
            $mes = $_POST['mes'];
            $anio = $_POST['anio'];
            $reg_civil = $_POST['reg_civil'];
            $exp_en = $_POST['exp_en'];
            $dir_resi = $_POST['dir_resi'];
            $tel_resi = $_POST['tel_resi'];
            $grdo_mat = $_POST['grdo_mat'];
            $enf_cronica = $_POST['enf_cronica'];
            $espc_enf = $_POST['espc_enf'];
            $fiebre_dolex = $_POST['fiebre_dolex'];
            $emerg_fami = $_POST['emerg_fami'];
            $nombre_fami = $_POST['nombre_fami'];
            $celular_fami = $_POST['celular_fami'];
            $apell_padre = $_POST['apell_padre'];
            $nom_padre = $_POST['nom_padre'];
            $ced_padre = $_POST['ced_padre'];
            $exp_padre = $_POST['exp_padre'];
            $lug_nac_padre = $_POST['lug_nac_padre'];
            $dia_padre = $_POST['dia_padre'];
            $anio_padre = $_POST['anio_padre'];
            $dir_padre = $_POST['dir_padre'];
            $tel_padre = $_POST['tel_padre'];
            $prof_padre = $_POST['prof_padre'];
            $emp_padre = $_POST['emp_padre'];
            $cargo_padre = $_POST['cargo_padre'];
            $dir_oficina_padre = $_POST['dir_oficina_padre'];
            $tel_oficina_padre = $_POST['tel_oficina_padre'];
            $email_padre = $_POST['email_padre'];
            $cel_padre = $_POST['cel_padre'];
            $apell_madre = $_POST['apell_madre'];
            $nom_madre = $_POST['nom_madre'];
            $ced_madre = $_POST['ced_madre'];
            $lug_nac_madre = $_POST['lug_nac_madre'];
            $dia_madre = $_POST['dia_madre'];
            $anio_madre = $_POST['anio_madre'];
            $dir_madre = $_POST['dir_madre'];
            $tel_madre = $_POST['tel_madre'];
            $prof_madre = $_POST['prof_madre'];
            $emp_madre = $_POST['emp_madre'];
            $cargo_madre = $_POST['cargo_madre'];
            $dir_oficina_madre = $_POST['dir_oficina_madre'];
            $tel_oficina_madre = $_POST['tel_oficina_madre'];
            $email_madre = $_POST['email_madre'];
            $cel_madre = $_POST['cel_madre'];
            $exp_madre = $_POST['exp_madre'];
            $nombre_alum = $_POST['nombre_alum'];
            $nivel_alum = $_POST['nivel_alum'];
            $resp_alum = $_POST['resp_alum'];
            $cedula_alum = $_POST['cedula_alum'];
            $exp_alum = $_POST['exp_alum'];
            $lug_nac_alum = $_POST['lug_nac_alum'];
            $dia_alum = $_POST['dia_alum'];
            $mes_alum = $_POST['mes_alum'];
            $anio_alum = $_POST['anio_alum'];
            $parentesco = $_POST['parentesco'];
            $id_estudiante = $_POST['id_estudiante'];
            $mes_padre = $_POST['mes_padre'];
            $mes_madre = $_POST['mes_madre'];

            $datos = array(
                'id_acudiente' => $id_acudiente,
                'apellidos' => $apellidos,
                'nombres' => $nombres,
                'lug_fecha' => $lug_fecha,
                'dia' => $dia,
                'mes' => $mes,
                'anio' => $anio,
                'reg_civil' => $reg_civil,
                'exp_en' => $exp_en,
                'dir_resi' => $dir_resi,
                'tel_resi' => $tel_resi,
                'grdo_mat' => $grdo_mat,
                'enf_cronica' => $enf_cronica,
                'espc_enf' => $espc_enf,
                'fiebre_dolex' => $fiebre_dolex,
                'emerg_fami' => $emerg_fami,
                'nombre_fami' => $nombre_fami,
                'celular_fami' => $celular_fami,
                'apell_padre' => $apell_padre,
                'nom_padre' => $nom_padre,
                'ced_padre' => $ced_padre,
                'exp_padre' => $exp_padre,
                'lug_nac_padre' => $lug_nac_padre,
                'dia_padre' => $dia_padre,
                'anio_padre' => $anio_padre,
                'dir_padre' => $dir_padre,
                'tel_padre' => $tel_padre,
                'prof_padre' => $prof_padre,
                'emp_padre' => $emp_padre,
                'cargo_padre' => $cargo_padre,
                'dir_oficina_padre' => $dir_oficina_padre,
                'tel_oficina_padre' => $tel_oficina_padre,
                'email_padre' => $email_padre,
                'cel_padre' => $cel_padre,
                'apell_madre' => $apell_madre,
                'nom_madre' => $nom_madre,
                'ced_madre' => $ced_madre,
                'exp_madre' => $exp_madre,
                'lug_nac_madre' => $lug_nac_madre,
                'dia_madre' => $dia_madre,
                'anio_madre' => $anio_madre,
                'dir_madre' => $dir_madre,
                'tel_madre' => $tel_madre,
                'prof_madre' => $prof_madre,
                'emp_madre' => $emp_madre,
                'cargo_madre' => $cargo_madre,
                'dir_oficina_madre' => $dir_oficina_madre,
                'tel_oficina_madre' => $tel_oficina_madre,
                'email_madre' => $email_madre,
                'cel_madre' => $cel_madre,
                'nombre_alum' => $nombre_alum,
                'nivel_alum' => $nivel_alum,
                'resp_alum' => $resp_alum,
                'cedula_alum' => $cedula_alum,
                'exp_alum' => $exp_alum,
                'lug_nac_alum' => $lug_nac_alum,
                'dia_alum' => $dia_alum,
                'mes_alum' => $mes_alum,
                'anio_alum' => $anio_alum,
                'parentesco' => $parentesco,
                'id_estudiante' => $id_estudiante,
                'mes_padre' => $mes_padre,
                'mes_madre' => $mes_madre
            );

            $guardar = ModeloMatricula::guardarFormatoModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("padres");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }


    public function agregarEstudianteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['nom_est']) &&
            !empty($_POST['nom_est'])
        ) {

            $datos = array(
                'id_acudiente' => $_POST['id_acudiente'],
                'nom_est' => $_POST['nom_est']
            );

            $guardar = ModeloMatricula::agregarEstudianteModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }


    public function guardarDocumentosControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente_arch']) &&
            !empty($_POST['id_acudiente_arch']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante']) &&
            !empty($_FILES['foto_est']['name']) &&
            !empty($_FILES['foto_est2']['name']) &&
            !empty($_FILES['volante']['name']) &&
            /* !empty($_FILES['cert_salud']['name']) && */
            !empty($_FILES['cert_eps']['name'])
        ) {

            if (!empty($_FILES['foto_est']['name'])) {
                $datos_est = array(
                    'archivo' => $_FILES['foto_est']['name'],
                    'tipo_doc' => 1,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'foto_est'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_est);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['foto_est2']['name'])) {
                $datos_est = array(
                    'archivo' => $_FILES['foto_est2']['name'],
                    'tipo_doc' => 1,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'foto_est2'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_est);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['volante']['name'])) {

                $datos_volante = array(
                    'archivo' => $_FILES['volante']['name'],
                    'tipo_doc' => 2,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'volante'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_volante);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['reg_civil']['name'])) {

                $datos_reg_civil = array(
                    'archivo' => $_FILES['reg_civil']['name'],
                    'tipo_doc' => 3,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'reg_civil'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_reg_civil);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['cert_salud']['name'])) {

                $datos_cert_salud = array(
                    'archivo' => $_FILES['cert_salud']['name'],
                    'tipo_doc' => 4,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'cert_salud'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_cert_salud);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['ex_visual']['name'])) {

                $datos_ex_visual = array(
                    'archivo' => $_FILES['ex_visual']['name'],
                    'tipo_doc' => 5,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'ex_visual'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_ex_visual);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['esq_vacuna']['name'])) {

                $datos_esq_vacuna = array(
                    'archivo' => $_FILES['esq_vacuna']['name'],
                    'tipo_doc' => 6,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'esq_vacuna'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_esq_vacuna);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['cert_eps']['name'])) {

                $datos_cert_eps = array(
                    'archivo' => $_FILES['cert_eps']['name'],
                    'tipo_doc' => 7,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'cert_eps'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_cert_eps);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['doc_custodia']['name'])) {

                $datos_doc_custodia = array(
                    'archivo' => $_FILES['doc_custodia']['name'],
                    'tipo_doc' => 8,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'doc_custodia'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_doc_custodia);
            }

            /*-------------------------------------------------------------*/

            if (!empty($_FILES['paz_salvo']['name'])) {

                $datos_paz_salvo = array(
                    'archivo' => $_FILES['paz_salvo']['name'],
                    'tipo_doc' => 9,
                    'id_acudiente' => $_POST['id_acudiente_arch'],
                    'id_estudiante' => $_POST['id_estudiante'],
                    'tipo' => 'paz_salvo'
                );

                $guardar_doc = $this->guardarDocumentoControl($datos_paz_salvo);
            }


            if ($guardar_doc == TRUE) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("../inicio");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        } else {
            echo '
            <script>
            ohSnap("Documentos requeridos incompletos!", { color: "red", "duration": "1000" });
            </script>
            ';
        }
    }


    public function guardarDocumentoControl($datos)
    {
        //obtener el nombre del archivo
        $nom_arch = $datos['archivo'];
        //extraer la extencion del archivo de el archivo
        $ext_arch = explode(".", $nom_arch);
        $ext_arch = end($ext_arch);


        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
        $fecha_arch = $d->format("YmdHisu");

        $nombre_archivo = strtolower(md5($datos['id_acudiente'] . '_' . $datos['id_estudiante'] . '_' . $datos['tipo_doc'] . '_' . $fecha_arch)) . '.' . $ext_arch;

        $datos_temp = array(
            'nombre' => $nombre_archivo,
            'tipo_doc' => $datos['tipo_doc'],
            'id_acudiente' => $datos['id_acudiente'],
            'id_estudiante' => $datos['id_estudiante']
        );

        $guardar_evidencia = ModeloMatricula::guardarDocumentoModel($datos_temp);

        if ($guardar_evidencia == TRUE) {
            //ruta donde de alojamiento el archivo
            $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
            $ruta_img = $carp_destino . $nombre_archivo;

            //verificar si subio el archivo y se mueve a su destino
            if (is_uploaded_file($_FILES[$datos['tipo']]['tmp_name'])) {
                move_uploaded_file($_FILES[$datos['tipo']]['tmp_name'], $ruta_img);
            }

            return TRUE;
        }
    }


    public function subirVolanteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            //obtener el nombre del archivo
            $nom_arch = $_FILES['volante']['name'];
            //extraer la extencion del archivo de el archivo
            $ext_arch = explode(".", $nom_arch);
            $ext_arch = end($ext_arch);


            $t = microtime(true);
            $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));
            $fecha_arch = $d->format("YmdHisu");

            $nombre_archivo = strtolower(md5($_POST['id_acudiente'] . '_' . $_POST['id_estudiante'] . '_' . $fecha_arch)) . '.' . $ext_arch;

            $datos_temp = array(
                'nombre' => $nombre_archivo,
                'id_acudiente' => $_POST['id_acudiente'],
                'id_estudiante' => $_POST['id_estudiante'],
                'id_log' => $_POST['id_log']
            );

            $guardar_evidencia = ModeloMatricula::subirVolanteModel($datos_temp);

            if ($guardar_evidencia == TRUE) {
                //ruta donde de alojamiento el archivo
                $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
                $ruta_img = $carp_destino . $nombre_archivo;

                //verificar si subio el archivo y se mueve a su destino
                if (is_uploaded_file($_FILES['volante']['tmp_name'])) {
                    move_uploaded_file($_FILES['volante']['tmp_name'], $ruta_img);
                }

                $datos_acudiente = $this->datosAcudienteControl($_POST['id_acudiente']);

                $mensaje = '<p>El siguiente correo es para informar que ya se encuentra disponible el volante de pago para el estudiante
                <span style="font-weight: bold;">' . $_POST['nombre'] . '</span>.</p>';

                $datos_correo = array(
                    'asunto' => 'Proceso de matricula',
                    //'correo' => 'jesuspolo00@gmail.com',
                    'correo' => $datos_acudiente['correo'],
                    'user' => $datos_acudiente['nombre'] . ' ' . $datos_acudiente['apellido'],
                    'mensaje' => $mensaje
                );

                $enviar = Correo::enviarCorreoModel($datos_correo);


                echo '
                <script>
                ohSnap("Subido correctamente!", { color: "green", "duration": "1000" });
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("volantes");
                }
                </script>
                ';
            }
        }
    }



    public function generarContratoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente']) &&
            isset($_POST['id_estudiante']) &&
            !empty($_POST['id_estudiante'])
        ) {

            $tipo_contrato = $_POST['tipo_contrato'];

            $valor = ($tipo_contrato == 1) ? '4535000' : '3932000';

            $datos = array(
                'id_estudiante' => $_POST['id_estudiante'],
                'id_acudiente' => $_POST['id_acudiente'],
                'valor' => $valor,
                'pagare' => $_POST['pagare'],
                'tipo' => $_POST['tipo_contrato'],
                'id_log' => $_POST['id_log']
            );

            $guardar = ModeloMatricula::generarContratoModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                ohSnap("Generado correctamente!", { color: "green", "duration": "1000" });
                window.open("' . BASE_URL . 'imprimir/contrato?estudiante=' . base64_encode($_POST['id_estudiante']) . '&acudiente=' . base64_encode($_POST['id_acudiente']) . '");
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }
}
