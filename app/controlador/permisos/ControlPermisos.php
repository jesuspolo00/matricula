<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPermisos
{

    private static $instancia;

    public static function singleton_permisos()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }


    public function permisosUsuarioControl($id_modulo, $id_opcion, $id_accion, $id_user)
    {
        $mostrar = ModeloPermisos::permisosUsuarioModel($id_modulo, $id_opcion, $id_accion, $id_user);
        return $mostrar;
    }
}