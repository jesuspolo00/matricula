<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuarios
{

    private static $instancia;

    public static function singleton_usuarios()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarUsuariosControl()
    {
        $mostrar = ModeloUsuarios::mostrarUsuariosModel();
        return $mostrar;
    }


    public function agregarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento']) &&
            isset($_POST['perfil']) &&
            !empty($_POST['perfil']) &&
            isset($_POST['password']) &&
            !empty($_POST['password']) &&
            isset($_POST['conf_password']) &&
            !empty($_POST['conf_password'])
        ) {
            $pass = $_POST['password'];
            $conf_pass = $_POST['conf_password'];

            if ($conf_pass == $pass) {

                $pass_hash = Hash::hashpass($conf_pass);

                $datos = array(
                    'documento' => $_POST['documento'],
                    'nombre' => $_POST['nombre'],
                    'apellido' => $_POST['apellido'],
                    'telefono' => $_POST['telefono'],
                    'correo' => $_POST['correo'],
                    'usuario' => $_POST['usuario'],
                    'perfil' => $_POST['perfil'],
                    'pass' => $pass_hash,
                    'id_log' => $_POST['id_log']
                );

                $guardar = ModeloUsuarios::agregarUsuarioModel($datos);

                if ($guardar == TRUE) {
                    echo '
                    <script>
                        ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                        setTimeout(recargarPagina,1050);
    
                    function recargarPagina(){
                        window.location.replace("index");
                    }
                    </script>
                    ';
                } else {
                    echo '
                    <script>
                        ohSnap("Error!", { color: "red", "duration": "1000" });
                    </script>
                    ';
                }
            } else {
                echo '
                <script>
                    ohSnap("Las contraseñas no coinciden!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }



    public function agregarAcudienteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {

            $pass_hash = Hash::hashpass('cronoplay123456@');

            $datos = array(
                'documento' => $_POST['documento'],
                'nombre' => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'correo' => $_POST['correo'],
                'telefono' => $_POST['telefono'],
                'perfil' => 2,
                'usuario' => $_POST['documento'],
                'pass' => $pass_hash,
                'id_log' => $_POST['id_log']
            );

            $guardar = ModeloUsuarios::agregarAcudienteModel($datos);

            if ($guardar == TRUE) {
                echo '
                <script>
                    ohSnap("Guardado correctamente!", { color: "green", "duration": "1000" });
                    setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                    ohSnap("Error!", { color: "red", "duration": "1000" });
                </script>
                ';
            }
        }
    }



    public function actualizarAcudienteControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_acudiente']) &&
            !empty($_POST['id_acudiente'])
        ) {

            $datos = array(
                'id_user' => $_POST['id_acudiente'],
                'documento' => $_POST['documento_edit'],
                'nombre' => $_POST['nombre_edit'],
                'apellido' => $_POST['apellido_edit'],
                'correo' => $_POST['correo_edit'],
                'telelfono' => $_POST['telefono_edit']
            );

            $actualizar = ModeloUsuarios::actualizarAcudienteModel($datos);

            if ($actualizar == TRUE) {
                echo '
                <script>
                    ohSnap("Actualizado correctamente!", { color: "green", "duration": "1000" });
                    setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                    <script>
                        ohSnap("Error!", { color: "red", "duration": "1000" });
                    </script>
                    ';
            }
        }
    }




    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuarios::inactivarUsuarioModel($datos);

            if ($buscar == TRUE) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {
            $id_user = $_POST['id_user'];
            $fecha = date('Y-m-d H:i:s');

            $datos = array('id_user' => $id_user, 'fecha' => $fecha);

            $buscar = ModeloUsuarios::activarUsuarioModel($datos);

            if ($buscar == TRUE) {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function verificarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['documento']) &&
            !empty($_POST['documento'])
        ) {
            $documento = $_POST['documento'];

            $buscar = ModeloUsuarios::buscarDocumentoModel($documento);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }



    public function verificarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['usuario']) &&
            !empty($_POST['usuario'])
        ) {
            $usuario = $_POST['usuario'];

            $buscar = ModeloUsuarios::verificarUsuarioModel($usuario);

            if ($buscar['id_user'] != "") {
                $rs = 'ok';
            } else {
                $rs = 'no';
            }

            return $rs;
        }
    }
}
